﻿using System;
using System.Text;

namespace HTools
{
    /// <summary>
    /// Hex conversions.
    /// </summary>
    public class Hex
    {
        #region [ Hex To Bin ]
        /// <summary>
        /// Hex to binary string. Return emptied string on error.
        /// </summary>
        /// <param name="hex">The hex.</param>
        /// <returns></returns>
        public static string ToBin(string hex)
        {
            return Hex.ToBin(hex, string.Empty);
        }

        /// <summary>
        /// Hex to binary string.
        /// </summary>
        /// <param name="hex">The hex.</param>
        /// <param name="valueOnError">The value on error.</param>
        /// <returns></returns>
        public static string ToBin(string hex, string valueOnError)
        {
            const long ERROR = long.MinValue + 1;
            long dec = Hex.ToLong(hex, ERROR);
            if (dec == ERROR)
            {
                return valueOnError;
            }
            string bin = Dec.ToBin(dec);
            return bin;
        }
        #endregion

        #region [ Hex To Byte ]
        /// <summary>
        /// Convert hex string into to a array of bytes.
        /// </summary>
        /// <param name="hex">The hex (Acceptable format FEC4A3B2, FE C4 A3 B2 or FE-C4-A4-B2).</param>
        /// <returns>Return null on error.</returns>
        public static byte[] ToBytes(string hex)
        {
            return Hex.ToBytes(hex, 0);
        }

        /// <summary>
        /// Convert hex string into to a array of bytes. Padded with zero if it is not long enough.
        /// </summary>
        /// <param name="hex">The hex.</param>
        /// <param name="totalLength">The total length.</param>
        /// <returns></returns>
        public static byte[] ToBytes(string hex, int totalLength)
        {
            try
            {
                hex = CleanHex(hex);

                if (hex.Length % 2 != 0)
                {
                    hex = "0" + hex;
                }

                int byteLength = hex.Length / 2;
                if (totalLength > byteLength)
                {
                    hex = Str.PadLeft(hex, totalLength * 2, '0');
                    byteLength = totalLength;
                }

                byte[] bytes = new byte[byteLength];
                for (int index = 0, i = 0; i < hex.Length; i += 2, index += 1)
                {
                    bytes[index] = (byte)(_hexLookupTable[hex[i] - '0'] << 4 |
                                      _hexLookupTable[hex[i + 1] - '0']);
                }
                return bytes;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static readonly int[] _hexLookupTable = new int[] 
                                {   
                                    0x00, 0x01, 0x02, 0x03, 0x04, 
                                    0x05, 0x06, 0x07, 0x08, 0x09,
                                    0x00, 0x00, 0x00, 0x00, 0x00, 
                                    0x00, 0x00, 0x0A, 0x0B, 0x0C, 
                                    0x0D, 0x0E, 0x0F 
                                };


        ///// <summary>
        ///// Hexes to bytes.
        ///// </summary>
        ///// <param name="str">The STR.</param>
        ///// <source>http://stackoverflow.com/questions/623104/c-byte-to-hex-string</source>
        ///// <returns></returns>
        //private static byte[] HexToBytes(string str)
        //{
        //    if (str.Length == 0 || str.Length % 2 != 0)
        //        return new byte[0];

        //    byte[] buffer = new byte[str.Length / 2];
        //    char c;
        //    for (int bx = 0, sx = 0; bx < buffer.Length; ++bx, ++sx)
        //    {
        //        // Convert first half of byte
        //        c = str[sx];
        //        buffer[bx] = (byte)((c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0')) << 4);

        //        // Convert second half of byte
        //        c = str[++sx];
        //        buffer[bx] |= (byte)(c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0'));
        //    }

        //    return buffer;
        //}
        #endregion

        #region [ Hex To Dec ]
        /// <summary>
        /// Covert hex string into int.
        /// </summary>
        /// <param name="hex">The hex.</param>
        /// <param name="valueOnError">The value on error.</param>
        /// <returns></returns>
        public static int ToInt(string hex, int valueOnError)
        {
            try
            {
                hex = CleanHex(hex);
                int value = Convert.ToInt32(hex, 16);
                return value;
            }
            catch
            {
                return valueOnError;
            }
        }

        /// <summary>
        /// Covert hex string into long.
        /// </summary>
        /// <param name="hex">The hex.</param>
        /// <param name="valueOnError">The value on error.</param>
        /// <returns></returns>
        public static long ToLong(string hex, long valueOnError)
        {
            try
            {
                hex = CleanHex(hex);
                throw new NotImplementedException ();
                //long value = Convert.ToInt64(hex, 16);
                //return value;
            }
            catch
            {
                return valueOnError;
            }
        }
        #endregion

        #region [ Hex To String ]
        /// <summary>
        /// Convert hex to bytes and bytes to ascii encoded string.
        /// </summary>
        /// <param name="hex">The hex.</param>
        /// <returns>
        /// Return emptied string on error.
        /// </returns>
        public static string ToString(string hex)
        {
            return Hex.ToString(hex, string.Empty);
        }

        /// <summary>
        /// Convert hex to bytes and bytes to ascii encoded string.
        /// </summary>
        /// <param name="hex">The hex.</param>
        /// <param name="valueOnError">The value on error.</param>
        /// <returns></returns>
        public static string ToString(string hex, string valueOnError)
        {
            // This function also clean the hex value
            byte[] b = Hex.ToBytes(hex);
            if (b != null)
            {
                string str = ByteArr.ToString(b);
                return str;
            }
            else
            {
                return valueOnError;
            }
        }
        #endregion

        #region [ Helper ]
        /// <summary>
        /// Cleans the hex string replace space and dash.
        /// </summary>
        /// <param name="hex">The hex.</param>
        /// <returns></returns>
        private static string CleanHex(string hex)
        {
            
            hex = Str.Replace(hex, "-", "").ToUpper();
            hex = Str.Replace(hex, " ", "").Trim();

            return hex;
        }
        #endregion
    }
}
