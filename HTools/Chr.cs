﻿using System;
using System.Text;

namespace HTools
{
    /// <summary>
    /// Functions to validate and manipulate character.
    /// </summary>
    public class Chr
    {
        /// <summary>
        /// Is the char 0 to 9 inclusive
        /// </summary>
        /// <param name="c">The character.</param>
        /// <returns>
        /// 	<c>true</c> if c is ASCII digit; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsAsciiDigit(char c)
        {
            return c >= '0' && c <= '9';
        }

        /// <summary>
        /// Is the char a-zA-Z
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static bool IsAsciiAlpha(char c)
        {
            return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
        }

        /// <summary>
        /// Is the char a-zA-Z0-9
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static bool IsAsciiAlphaNumeric(char c)
        {
            return IsAsciiDigit(c) || IsAsciiAlpha(c);
        }
    }
}
