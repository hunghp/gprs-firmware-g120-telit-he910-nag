﻿using System;
using System.Text;

namespace HTools
{
    /// <summary>
    /// Byte[] conversion.
    /// </summary>
    public class ByteArr
    {
        #region [ Bytes To Bin ]
        /// <summary>
        /// Convert bytes to binary string. Return emptied string on error.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns></returns>
        public static string ToBin(byte[] bytes)
        {
            string hex = ByteArr.ToHex(bytes);
            return Hex.ToBin(hex);
        }
        #endregion

        #region [ Bytes To Dec ]
        /// <summary>
        /// Convert the hex value in the byte into an int.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <param name="valueOnError">The value on error.</param>
        /// <returns></returns>
        public static int ToInt(byte[] bytes, int valueOnError)
        {
            try
            {
                return (int)ByteArr.ToLong(bytes, valueOnError);
            }
            catch
            {
                return valueOnError;
            }
        }

        /// <summary>
        /// Convert the hex value in the byte into an long value.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <param name="valueOnError">The value on error.</param>
        /// <returns></returns>
        public static long ToLong(byte[] bytes, long valueOnError)
        {
            try
            {
                string hex = ByteArr.ToHex(bytes);
                if (Str.IsEmpty(hex))
                {
                    return valueOnError;
                }

                long result = Hex.ToLong(hex, valueOnError);
                return result;
            }
            catch
            {
                // This happend when the value of long is exceeded the Min/MaxValue
                return valueOnError;
            }
        }
        #endregion

        #region [ Bytes To Hex ]
        /// <summary>
        /// Convert bytes into hex.
        /// </summary>
        /// <param name="bytes"></param>
        /// <source>http://stackoverflow.com/questions/623104/c-byte-to-hex-string</source>
        /// <returns></returns>
        public static string ToHex(byte[] bytes)
        {
            char[] c = new char[bytes.Length * 2];

            byte b;

            for (int bx = 0, cx = 0; bx < bytes.Length; ++bx, ++cx)
            {
                b = ((byte)(bytes[bx] >> 4));
                c[cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);

                b = ((byte)(bytes[bx] & 0x0F));
                c[++cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);
            }

            return new string(c);
        }
        #endregion

        #region [ Bytes To String ]
        /// <summary>
        /// Bytes to ascii encoded string.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns>
        /// </returns>
        public static string ToString(byte[] bytes)
        {
            char[] chars = Encoding.UTF8.GetChars(bytes);
            return new string(chars);
        }
        #endregion




        #region [ GetCreateString ]
        /// <summary>
        /// Return string representation of array byte in "new byte[]{ }" format.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns></returns>
        public static string GetCreateString(byte[] bytes)
        {
            string result = "new byte[] { ";
            foreach (byte b in bytes)
            {
                result += ((int)b).ToString() + ", ";
            }
            result = result.TrimEnd(',', ' ');
            result += " };";
            return result;
        }
        #endregion
    }
}
