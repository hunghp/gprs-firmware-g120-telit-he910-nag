using System;
using Microsoft.SPOT;

namespace GPS_GSM_Unit
{
    public class Crc16
    {
        const ushort polynomial = 0xA001;
        static ushort[] table = new ushort[256];

        /// <summary>
        /// Computes the CRC16 checksum for the specified bytes.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns></returns>
        public static ushort Compute(byte[] bytes)
        {
            ushort crc = 0;
            for (int i = 0; i < bytes.Length; ++i)
            {
                byte index = (byte)(crc ^ bytes[i]);
                crc = (ushort)((crc >> 8) ^ table[index]);
            }
            return crc;
        }

        public static byte[] ComputeBytes(byte[] bytes)
        {
            ushort crc = Compute(bytes);
            return BitConverter.GetBytes(crc);
        }

        static Crc16()
        {
            ushort value;
            ushort temp;
            for (ushort i = 0; i < table.Length; ++i)
            {
                value = 0;
                temp = i;
                for (byte j = 0; j < 8; ++j)
                {
                    if (((value ^ temp) & 0x0001) != 0)
                    {
                        value = (ushort)((value >> 1) ^ polynomial);
                    }
                    else
                    {
                        value >>= 1;
                    }
                    temp >>= 1;
                }
                table[i] = value;
            }
        }

        public static ushort SwapEndian(ushort v)
        {
            return (ushort)(((v & 0xff) << 8) | ((v >> 8) & 0xff));
        }
    }
}
