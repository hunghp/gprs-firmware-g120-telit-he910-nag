using System;
using Microsoft.SPOT;

namespace GPS_GSM_Unit
{
    class SpecialGlobalVar
    {
        public static bool _SystemNeedReboot = false;
        public static int _TCPConnectionRetries = 0;    //the number of failed attempts to connect to the hub using TCP connection using the GSM unit
        public static bool _SystemRebootByWatchDog = false;
        public static bool _SystemRebootNotByWatchDog = false;       
        public static bool _3GModuleSerialPortBusy = false; //Indicate the serial port connected to the 3G module is busy
        public static bool _GPSThreadIsOnHold = false;
        //Create static buffer to serial ports        
        public static byte[] _3GModuleSerialBuffer = new byte[2048];
        public static byte[] _XBEE900SerialBuffer = new byte[75];
        public static int _COM3TotalBytesRead = 0;
    }
}
