using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.IO;

using GHI.IO;
using GHI.IO.Storage;



namespace GPS_GSM_Unit
{
    public enum LOG_FILE_TYPE : int
    {
        REGULAR_LOG_FILE = 0,
        SENSOR_DOWNLOAD_LOG_FILE = 1
    }

    public class SDStorage 
    {
        SDCard sd_card = null;

        public SDStorage(SDCard sdCard)
        {
            this.sd_card = sdCard;           
        }        
       

        public bool WriteUndeliverableDataToDataLogFile(ArrayList strDataArray, int logFileIndex, out int newLogFileIndex, int maxLogFileSize, LOG_FILE_TYPE logFileType)
        {                                                            
            bool createdNewLogFile = false;            
            FileInfo fileInfo = null;
                           
            try
            {
                string rootDirectory = VolumeInfo.GetVolumes()[0].RootDirectory;
                string logFolderPath = null;
                string logFilePath = null;

                if (logFileType == LOG_FILE_TYPE.REGULAR_LOG_FILE)
                {
                    logFolderPath = @"\Log";
                    logFilePath = @"\Log\log_";
                }
                else 
                {
                    logFolderPath = @"\DL_Log";
                    logFilePath = @"\DL_Log\log_";
                }
                
                //if directory does not exist create it
                if (!Directory.Exists(rootDirectory + logFolderPath))
                {
                    Directory.CreateDirectory(rootDirectory + logFolderPath);
                }
                                        
                //Check to see what's the current log file is, if it exist and if it's size exceed limit
                //if size exceed limit, create a new log file by incrementing the log index counter
                //This loop takes care of power lost or system restart, it will loop through logFileIndex from 0 until if find the last file and set the logFileIndex to that and continue
                createdNewLogFile = false;
                                
                while (true)
                {
                    if (File.Exists(rootDirectory + logFilePath + logFileIndex.ToString() + ".txt"))
                    {
                        //Check to see if this file exceed limit size
                        fileInfo = new FileInfo(rootDirectory + logFilePath + logFileIndex.ToString() + ".txt");
                        if (fileInfo.Length > maxLogFileSize)
                        {
                            logFileIndex++;     //check next file index
                            continue;
                        }
                        else  //this is the last file, so we should append to this, exit loop
                        {
                            createdNewLogFile = false;
                            break;
                        }
                    }
                    else // this is the next new log file index, we should create this file
                    {
                        createdNewLogFile = true;
                        break;
                    }
                }
                              
                if (createdNewLogFile)
                {
                    using (var fs = new FileStream(rootDirectory + logFilePath + logFileIndex.ToString() + ".txt", FileMode.Create, FileAccess.Write))
                    {
                        foreach (var item in strDataArray)
                        {
                            if (item == null)
                                continue;
                            if (String.Compare(item.ToString(), "") == 0)
                                continue;
                            try
                            {
                                byte[] data = Encoding.UTF8.GetBytes(item.ToString());
                                fs.Write(data, 0, data.Length);
                                byte[] byteCRLF = new byte[] { 13, 10 };
                                fs.Write(byteCRLF, 0, byteCRLF.Length);
                            }
                            catch (Exception ex)
                            {
                                Debug.Print("From [WriteUndeliverableDataToDataLogFile, writing for file stream]: " + ex.ToString());

                                //System needs restart, set watchdog timer flag            
                                SpecialGlobalVar._SystemNeedReboot = true;

                                newLogFileIndex = 0;

                                return false;
                            }
                        }
                        fs.Flush();
                        fs.Close();
                    }

                    //Flush all
                    VolumeInfo.GetVolumes()[0].FlushAll();
                }
                else
                {
                    using (var fs = new FileStream(rootDirectory + logFilePath + logFileIndex.ToString() + ".txt", FileMode.Append, FileAccess.Write))
                    {
                        foreach (var item in strDataArray)
                        {
                            if (item == null)
                                continue;
                            if (String.Compare(item.ToString(), "") == 0)
                                continue;

                            byte[] data = Encoding.UTF8.GetBytes(item.ToString());
                            fs.Write(data, 0, data.Length);
                            byte[] byteCRLF = new byte[] { 13, 10 };
                            fs.Write(byteCRLF, 0, byteCRLF.Length);
                        }
                        fs.Flush();
                        fs.Close();
                    }
                }
                                                                                       

                //Assigned new log file index counter
                newLogFileIndex = logFileIndex;

                //force garbage collection
                Debug.GC(true);
                                                 
                return true;
            }
            catch (Exception ex)
            {
                Debug.Print("From [WriteUndeliverableDataToDataLogFile]: " + ex.ToString());  
                
                //System needs restart, set watchdog timer flag            
                SpecialGlobalVar._SystemNeedReboot = true;

                newLogFileIndex = 0;

                return false;
            }           
        }

        public ArrayList ReadDataLogFile(LOG_FILE_TYPE logFileType)
        {                                                    
            byte[] readBytes = null;            
            ArrayList dataLogList = null;
            
            //initialize array list
            try
            {
                dataLogList = new ArrayList();
            }
            catch (Exception ex)
            {
                Debug.Print("From [ReadDataLogFile], initilize array list: " + ex.ToString());                                              

                //System needs restart, set watchdog timer flag   
                SpecialGlobalVar._SystemNeedReboot = true;

                return null;
            }

            //check for existing directory and if it has any files           
            string rootDirectory = VolumeInfo.GetVolumes()[0].RootDirectory;
            string logFolderPath = null;            

            if (logFileType == LOG_FILE_TYPE.REGULAR_LOG_FILE)
            {
                logFolderPath = @"\Log";                
            }
            else
            {
                logFolderPath = @"\DL_Log";                
            }
   

            //if directory does not exist create it
            if (!Directory.Exists(rootDirectory + logFolderPath))
            {
                Directory.CreateDirectory(rootDirectory + logFolderPath);
                return null;
            }

            try
            {
                rootDirectory = VolumeInfo.GetVolumes()[0].RootDirectory;                                           
                //if Log directory is empty, return empty list
                if (Directory.GetFiles(rootDirectory + logFolderPath).Length == 0)
                {                                                          
                    return null;
                }              
            }
            catch (Exception ex)
            {
                Debug.Print("From [ReadDataLogFile], get root directory check directory files length: " + ex.ToString());                                              

                //System needs restart, set watchdog timer flag   
                SpecialGlobalVar._SystemNeedReboot = true;

                return null;
            }
           
            //get all files from directory if exist
            string[] logFiles;
            try
            {
                logFiles = Directory.GetFiles(rootDirectory + logFolderPath);                
            }
            catch(Exception ex)
            {
                Debug.Print("From [ReadDataLogFile], using Directory.GetFiles to get all files: " + ex.ToString());                                              

                //System needs restart, set watchdog timer flag   
                SpecialGlobalVar._SystemNeedReboot = true;

                return null;
            }
            
            using (var fs = new FileStream(logFiles[0], FileMode.Open, FileAccess.Read))
            {
                int byteToRead = (int)fs.Length;
                readBytes = new byte[byteToRead];
                fs.Read(readBytes, 0, byteToRead);
                fs.Flush();
                fs.Close();
            }
            
             
            try
            {
                HUtility util = new HUtility();
                
                readBytes = util.SanitizeForUTF8(readBytes); //santized for valid utf8 character first
                readBytes = util.FilterForValidHexBytes(readBytes); //filter for valid hex bytes only and replace everything else with line feed byte
                string fileData = new string(Encoding.UTF8.GetChars(readBytes));               
                
                //Delete the read file
                File.Delete(logFiles[0]);                
                                               
                string[] dataPakageStr = fileData.Split('\n');
                foreach (string item in dataPakageStr)
                {
                    if (String.Compare(item, "") == 0)
                        continue;
                    dataLogList.Add(item.Trim());
                }               

                return dataLogList;
            }
            catch(Exception ex)
            {
                Debug.Print("From [ReadDataLogFile], return log array list: " + ex.ToString());                                                             
                return null;
            }
        }

        public bool WriteToSettingFile(string strData, bool append)
        {            
            try
            {
                string rootDirectory = VolumeInfo.GetVolumes()[0].RootDirectory;
                if (append)
                {                    
                    using (var fs = new FileStream(rootDirectory + @"\Settings.txt", FileMode.Append, FileAccess.Write))
                    {
                        byte[] byteCRLF = new byte[] { 13, 10 };
                        byte[] data = Encoding.UTF8.GetBytes(strData);
                        fs.Write(byteCRLF, 0, byteCRLF.Length);
                        fs.Write(data, 0, data.Length);
                        fs.Write(byteCRLF, 0, byteCRLF.Length);
                        fs.Flush();
                        fs.Close();
                    }
                }
                else
                {                    
                    using (var fs = new FileStream(rootDirectory + @"\Settings.txt", FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                    {
                        byte[] byteCRLF = new byte[] { 13, 10 };
                        byte[] data = Encoding.UTF8.GetBytes(strData);                        
                        fs.Write(data, 0, data.Length);
                        fs.Write(byteCRLF, 0, byteCRLF.Length);
                        fs.Flush();
                        fs.Close();
                    }
                }

                //force garbage collection
                Debug.GC(true);

                return true;
            }
            catch (Exception ex)
            {
                Debug.Print("From [WriteToSettingFile]: " + ex.ToString());                                
                
                //System needs restart, set watchdog timer flag  
                SpecialGlobalVar._SystemNeedReboot = true;

                return false;
            }           
        }

        public string[] ReadSettingFile()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("PROCESS_GPS_DATA_INTERVAL = 5");
            sb.AppendLine("DEVICE_NAME = New Unit");
            sb.AppendLine("TRANSMIT_INTERVAL = 5");
            sb.AppendLine("WIRELESS_SENSOR_OFFLINE_THRESHOLD = 10");
            sb.AppendLine("IP = 0.0.0.0");
            sb.AppendLine("PORT = 9000");
            sb.AppendLine("DEVICE_PHONE_NO = None");
            sb.AppendLine("APN_NAME = None");
            sb.AppendLine("APN_USERNAME = None");
            sb.AppendLine("APN_PASSWORD = None");
            sb.AppendLine("EPOCH_TIME_STAMP = 327024000");
            sb.AppendLine("SETTING_ID = 0");
            sb.AppendLine("SENSORS_DEFINITION = gprs*gprs_four_port_v1__4*DEVICE_SERIAL*gprs_hw_900:io1,temp:io2,temp:io3,on_off:io4,zero_to_five");
           
            byte[] data = null;                            
            byte[] readData = null;
            string[] readSettings = null;
           
            try
            {                               
                string rootDirectory = VolumeInfo.GetVolumes()[0].RootDirectory;

                if (!File.Exists(rootDirectory + @"\Settings.txt"))
                {                    
                    using (var fs = new FileStream(rootDirectory + @"\Settings.txt", FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        byte[] byteCRLF = new byte[] { 13, 10 };
                        
                        data = Encoding.UTF8.GetBytes(sb.ToString());
                        fs.Write(data, 0, data.Length);
                        fs.Write(byteCRLF, 0, byteCRLF.Length);
                        
                        fs.Flush();
                        fs.Close();
                    }                  
                    

                    data = null;                    
                    return sb.ToString().Split('\n');   //return default settings
                }
                else
                {                   
                    using (var fs = new FileStream(rootDirectory + @"\Settings.txt", FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        readData = new byte[2048];
                        int read_count = fs.Read(readData, 0, readData.Length);

                        readSettings = new string(Encoding.UTF8.GetChars(readData), 0, read_count).Split('\n');                       
                    }                  
                                                         
                    readData = null;
                   
                    return readSettings;
                }
            }
            catch (Exception ex)
            {
                Debug.Print("From [ReadSettingFile]: " + ex.ToString());                                    

                //System needs restart, set watchdog timer flag
                SpecialGlobalVar._SystemNeedReboot = true;

                return null;
            }                       
        }        

        public bool WriteIncompleteDownloadListToFile(ArrayList incompleteDownloadList)
        {
            try
            {
                string rootDirectory = VolumeInfo.GetVolumes()[0].RootDirectory;                
                
                using (var fs = new FileStream(rootDirectory + @"\IncompleteDownloadList.txt", FileMode.Create, FileAccess.Write))
                {
                    IncompleteDownloadRecord package = null;
                    string packageString = null;
                    foreach (var item in incompleteDownloadList)
                    {
                        package = item as IncompleteDownloadRecord;
                        packageString = package.SensorID.Trim() + "," + package.StartDate.Year.ToString() + "," + package.StartDate.Month.ToString() + "," + package.StartDate.Day.ToString() + "," +
                                        package.StartDate.Hour.ToString() + "," + package.StartDate.Minute.ToString() + "," + package.StartDate.Second.ToString() + "," +
                                        package.EndDate.Year.ToString() + "," + package.EndDate.Month.ToString() + "," + package.EndDate.Day.ToString() + "," + package.EndDate.Hour.ToString() + "," +
                                        package.EndDate.Minute.ToString() + "," + package.EndDate.Second.ToString() + "," + package.StartMemBlock.ToString() + "," + package.EndMemBlock.ToString();                        

                        byte[] byteCRLF = new byte[] { 13, 10 };
                        byte[] data = Encoding.UTF8.GetBytes(packageString);                        
                        fs.Write(data, 0, data.Length);
                        fs.Write(byteCRLF, 0, byteCRLF.Length);                        
                    }
                    fs.Flush();
                    fs.Close();
                }
                

                //force garbage collection
                Debug.GC(true);

                return true;
            }
            catch (Exception ex)
            {
                Debug.Print("From [WriteIncompleteDownloadListToFile]: " + ex.ToString());

                //System needs restart, set watchdog timer flag  
                SpecialGlobalVar._SystemNeedReboot = true;

                return false;
            }           
        }

        public string[] ReadIncompleteDownloadListFromFile()
        {
            byte[] readBytes = null; 
            try
            {                
                string rootDirectory = VolumeInfo.GetVolumes()[0].RootDirectory;
                //Check to see file exist
                if (File.Exists(rootDirectory + @"\IncompleteDownloadList.txt") == false)
                {
                    return null;
                }

                using (var fs = new FileStream(rootDirectory + @"\IncompleteDownloadList.txt", FileMode.Open, FileAccess.Read))
                {
                    int byteToRead = (int)fs.Length;
                    readBytes = new byte[byteToRead];
                    fs.Read(readBytes, 0, byteToRead);
                    fs.Flush();
                    fs.Close();
                }

                if (readBytes == null)
                    return null;

                string fileData = new string(Encoding.UTF8.GetChars(readBytes));                

                string[] dataPakageStr = fileData.Trim().Split(new char[] {'\r', '\n'});
                
                return dataPakageStr;
            }
            catch (Exception ex)
            {
                Debug.Print("From [ReadIncompleteDownloadListFromFile]: " + ex.ToString());

                //System needs restart, set watchdog timer flag  
                SpecialGlobalVar._SystemNeedReboot = true;

                return null;
            }       
                                                                     
        }


    }
}
