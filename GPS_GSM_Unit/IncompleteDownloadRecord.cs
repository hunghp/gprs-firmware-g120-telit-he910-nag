using System;
using Microsoft.SPOT;

namespace GPS_GSM_Unit
{
    public class IncompleteDownloadRecord
    {
        public string SensorID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int StartMemBlock { get; set; }
        public int EndMemBlock { get; set; }
        public IncompleteDownloadRecord(string sensorID, DateTime startDate, DateTime endDate, int startMemBlock, int endMemBlock)
        {
            this.SensorID = sensorID;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.StartMemBlock = startMemBlock;
            this.EndMemBlock = endMemBlock;
        }

        public override string ToString()
        {
            return "SensorID: " + this.SensorID + ", " +
                   "StartDate: " + this.StartDate.ToString() + ", " +
                   "EndDate: " + this.EndDate.ToString() + ", " +
                   "StartMemBlock: " + this.StartMemBlock.ToString("X") + ", " +
                   "EndMemBlock: " + this.EndMemBlock.ToString("X");
        }
    }
}
