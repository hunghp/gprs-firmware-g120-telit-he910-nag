using System;
using Microsoft.SPOT;

namespace GPS_GSM_Unit
{
    public class MyByte
    {
        public byte TheByte { get; set; }
        public MyByte(byte the_byte)
        {
            this.TheByte = the_byte;
        }
    }
}
