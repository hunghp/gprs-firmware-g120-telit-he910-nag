using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using System.Threading;

namespace GPS_GSM_Unit
{
    class DS18B20
    {
        public const byte SearchROM = 0xF0;
        public const byte ReadROM = 0x33;
        public const byte MatchROM = 0x55;
        public const byte SkipROM = 0xCC;
        public const byte AlarmSearch = 0xEC;
        public const byte StartTemperatureConversion = 0x44;
        public const byte ReadScratchPad = 0xBE;
        public const byte WriteScratchPad = 0x4E;
        public const byte CopySratchPad = 0x48;
        public const byte RecallEEPROM = 0xB8;
        public const byte ReadPowerSupply = 0xB4;

        private OneWire one_wire;

        public DS18B20(Microsoft.SPOT.Hardware.OutputPort dataPin)
        {
            one_wire = new OneWire(dataPin);

            //Set to 12 bits
            if (one_wire.TouchReset() == 1)
            {
                one_wire.WriteByte(SkipROM);
                one_wire.WriteByte(WriteScratchPad);
                one_wire.WriteByte(0x00);
                one_wire.WriteByte(0x00);
                one_wire.WriteByte(0x7F);
                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Get 2 bytes raw data for temperature
        /// </summary>
        /// <returns>raw temp</returns>
        public int GetRawTemperature()
        {
            int rawTemp = 0;
           
            if (one_wire.TouchReset() == 1)
            {
                one_wire.WriteByte(SkipROM); // Skip ROM
                one_wire.WriteByte(StartTemperatureConversion); // Start temperature conversion

                
                while (one_wire.ReadByte() == 0) { }

                one_wire.TouchReset();
                one_wire.WriteByte(SkipROM); // Skip ROM
                one_wire.WriteByte(ReadScratchPad); // Read Scratchpad                 

                rawTemp = one_wire.ReadByte();            // LSB
                rawTemp |= one_wire.ReadByte() << 8;    // MSB 

                return rawTemp;
            }
            return 999;
        }        

        /// <summary>
        /// Get Celsius temp from sensor
        /// </summary>
        /// <returns>return -200 if failed</returns>
        public double GetTempInCelsius()
        {              
            double tempInC = 0;
            int rawTemp = GetRawTemperature();          
            
            if (rawTemp != 999)
            {
                //Get the sign bit 
                if (GetSignBit(rawTemp) == 1)
                {
                    tempInC = (double)(rawTemp / 16F);
                }
                else
                {
                    //Convert from 2's complement
                    rawTemp = ~rawTemp + 1;

                    byte[] rawTempBytes = BitConverter.GetBytes(rawTemp);
                    uint temp = BitConverter.ToUInt16(rawTempBytes, 0);                 
                    
                    tempInC = (double)(temp / 16F);
                    tempInC *= -1;
                }

                return tempInC;
            }
            else
            {
                return 200;
            }
        }

        /// <summary>
        /// Get the sign bit
        /// </summary>
        /// <param name="rawTemp"></param>
        /// <returns>returns -1 if negative temp, and 1 if positive temp</returns>
        public int GetSignBit(int rawTemp)
        {
            int signBits = rawTemp >> 12;
            if (signBits == 15)
                return -1;
            else
                return 1;

        }

        public void Dispose()
        {
            one_wire = null;
            Debug.GC(true);
        }
    }
}
