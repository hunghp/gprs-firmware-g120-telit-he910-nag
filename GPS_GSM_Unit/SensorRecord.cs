using System;
using Microsoft.SPOT;

namespace GPS_GSM_Unit
{
    // io_flag is used to indicate which io is valid for sending
    // io_flag = 1, only io1 is valid 
    // io_flag = 2, only io2 is valid
    // io_flag = 3, io1 and io2 are both valid
    public class SensorRecord
    {
        public SensorRecord(string sensorID, int transmitPeriod, DateTime timeStamp, int raw_iovalue_1, int raw_iovalue_2, string type, int battery, int alarmStatus,
                         int blockIndex, int recordIndex, int sensorClock, int ioFlag, int defIndex)
        {
            this.SensorID = sensorID;
            this.TransmitPeriod = transmitPeriod;
            this.TimeStamp = timeStamp;
            this.RawIOValue1 = raw_iovalue_1;
            this.RawIOValue2 = raw_iovalue_2;
            this.Type = type;
            this.Battery = battery;
            this.AlarmStatus = alarmStatus;
            this.BlockIndex = blockIndex;
            this.RecordIndex = recordIndex;
            this.SensorClock = sensorClock;
            this.IOFlag = ioFlag;
            this.DefIndex = defIndex;
        }

        public string SensorID { get; set; }
        public int TransmitPeriod { get; set; }
        public DateTime TimeStamp { get; set; }
        public int RawIOValue1 { get; set; }
        public int RawIOValue2 { get; set; }
        public string Type { get; set; }
        public int Battery { get; set; }
        public int AlarmStatus { get; set; }
        public int BlockIndex { get; set; }
        public int RecordIndex { get; set; }
        public int SensorClock { get; set; }
        public int IOFlag { get; set; }  //This is a flag to indicate which io is valid according to the configuration by the user
        public int DefIndex { get; set; }

        public override string ToString()
        {
            return "SensorID: " + this.SensorID + ", " +
                   "TransmitPeriod: " + this.TransmitPeriod.ToString() + ", " +
                   "TimeStamp: " + this.TimeStamp.ToString() + ", " +
                   "RawIO1: " + this.RawIOValue1.ToString() + ", " +
                   "RawIO2: " + this.RawIOValue2.ToString() + ", " +
                   "Type: " + this.Type + ", " +
                   "Battery: " + this.Battery.ToString() + ", " +
                   "Alarm: " + this.AlarmStatus.ToString() + ", " +
                   "BlockIndex: " + this.BlockIndex.ToString() + ", " +
                   "RecordIndex: " + this.RecordIndex.ToString() + ", " +
                   "SensorClock: " + this.SensorClock.ToString() + ", " +
                   "IO Flag:" + this.IOFlag.ToString() + "\n" + ", " +
                   "Definition Index:" + this.DefIndex.ToString() + "\n";
        }
    }
}
