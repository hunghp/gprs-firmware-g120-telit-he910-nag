using System;
using Microsoft.SPOT;
using System.Collections;
using GHI.SQLite;

namespace GPS_GSM_Unit
{
    class DB
    {
        private Database myDatabase = null;
        public DB()
        {
            //Empty constructor         
        }

        public bool OpenDB()
        {
            try
            {
                myDatabase = new Database("\\SD\\DB.s3db");
                return true;
            }
            catch
            {
                return false;
            }

        }

        public void CloseDB()
        {
            myDatabase.Dispose();
        }

        public ArrayList GetSensorList()
        {
            ArrayList sensorList = new ArrayList();
            string sn = null;
            int transmit_period = 0;
            string timeStamp;
            int io1 = 0;
            int io2 = 0;
            string type = null;
            int bat = 0;
            int alarm = 0;
            int blockIndex = 0;
            int recordIndex = 0;
            int sensorClk = 0;
            int ioFlag = 0;
            int defIndex = 0;

            try
            {
                //Get Wireless Sensor List
                ResultSet result = myDatabase.ExecuteQuery("SELECT * FROM SENSORS");
                sensorList = new ArrayList();
                for (int i = 0; i < result.RowCount; i++)
                {
                    sn = result[i, 0].ToString();
                    transmit_period = Convert.ToInt32(result[i, 1].ToString());
                    timeStamp = result[i, 2].ToString();
                    io1 = Convert.ToInt32(result[i, 3].ToString());
                    io2 = Convert.ToInt32(result[i, 4].ToString());
                    type = result[i, 5].ToString();
                    bat = Convert.ToInt32(result[i, 6].ToString());
                    alarm = Convert.ToInt32(result[i, 7].ToString());
                    blockIndex = Convert.ToInt32(result[i, 8].ToString());
                    recordIndex = Convert.ToInt32(result[i, 9].ToString());
                    sensorClk = Convert.ToInt32(result[i, 10].ToString());
                    ioFlag = Convert.ToInt32(result[i, 11].ToString());
                    defIndex = Convert.ToInt32(result[i, 12].ToString());

                    sensorList.Add(new SensorRecord(sn, transmit_period, ConvertStringToDateTime(timeStamp), io1, io2, type, bat, alarm, blockIndex, recordIndex, sensorClk, ioFlag, defIndex));
                }


                //Get Hardwired ports
                result = myDatabase.ExecuteQuery("SELECT * FROM PORTS");
                for (int i = 0; i < result.RowCount; i++)
                {
                    sensorList.Add(new GPRSPort(result[i, 0].ToString(), Convert.ToInt32(result[i, 1].ToString())));
                }

                return sensorList;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateSensorRecord(SensorRecord sr)
        {
            string sn = sr.SensorID.Trim();
            string transmit_period = sr.TransmitPeriod.ToString();
            string timeStamp = ConvertDateTimeToString(sr.TimeStamp);
            string io1 = sr.RawIOValue1.ToString();
            string io2 = sr.RawIOValue2.ToString();
            string type = sr.Type;
            string bat = sr.Battery.ToString();
            string alarm = sr.AlarmStatus.ToString();
            string blockIndex = sr.BlockIndex.ToString();
            string recordIndex = sr.RecordIndex.ToString();
            string sensorClk = sr.SensorClock.ToString();

            string query = "UPDATE SENSORS SET Transmit_Period = " + transmit_period + ", Last_Received = '" + timeStamp + "', IOValue1 = " + io1 + ", IOValue2 = " + io2 +
                           ", Type = '" + type + "', Battery = " + bat + ", Alarm = " + alarm + ", BlockIndex = " + blockIndex + ", RecordIndex = " + recordIndex + ", SensorClock = " + sensorClk +
                           " WHERE ID = '" + sn + "'";
            try
            {
                myDatabase.ExecuteNonQuery(query);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertSensorRecord(SensorRecord sr, int DefinitionIndex)
        {
            string sn = sr.SensorID.Trim();
            string transmit_period = sr.TransmitPeriod.ToString();
            string timeStamp = ConvertDateTimeToString(sr.TimeStamp);
            string io1 = sr.RawIOValue1.ToString();
            string io2 = sr.RawIOValue2.ToString();
            string type = sr.Type;
            string bat = sr.Battery.ToString();
            string alarm = sr.AlarmStatus.ToString();
            string blockIndex = sr.BlockIndex.ToString();
            string recordIndex = sr.RecordIndex.ToString();
            string sensorClk = sr.SensorClock.ToString();
            string ioflag = sr.IOFlag.ToString();

            string query = "INSERT INTO SENSORS VALUES ('" + sn + "'," + transmit_period + ",'" + timeStamp + "'," + io1 + "," + io2 + ",'" + type + "'," + bat + "," + alarm + "," + blockIndex + "," + recordIndex + "," +
                           sensorClk + "," + ioflag + "," + DefinitionIndex + ")";
            try
            {
                myDatabase.ExecuteNonQuery(query);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertPortRecord(GPRSPort port, int DefinitionIndex)
        {
            string query = "INSERT INTO PORTS VALUES ('" + port.PortName.Trim() + "'," + DefinitionIndex +")";
            try
            {
                myDatabase.ExecuteNonQuery(query);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool TruncateDBTables()
        {
            try
            {
                myDatabase.ExecuteNonQuery("DELETE FROM SENSORS");
                myDatabase.ExecuteNonQuery("DELETE FROM PORTS");
                return true;
            }
            catch
            {
                return false;
            }
        }

        public DateTime ConvertStringToDateTime(string datetime)
        {
            string[] dt1 = datetime.Split(' ');

            string[] date = dt1[0].Split('-');
            int year = Convert.ToInt32(date[0].Trim());
            int month = Convert.ToInt32(date[1].Trim());
            int day = Convert.ToInt32(date[2].Trim());

            string[] time = dt1[1].Split(':');
            int hh = Convert.ToInt32(time[0].Trim());
            int mm = Convert.ToInt32(time[1].Trim());
            int ss = Convert.ToInt32(time[2].Trim());

            return new DateTime(year, month, day, hh, mm, ss);
        }

        public string ConvertDateTimeToString(DateTime dt)
        {
            string year = dt.Year.ToString();
            string month = dt.Month.ToString();
            string day = dt.Day.ToString();

            string hour = dt.Hour.ToString();
            string minute = dt.Minute.ToString();
            string second = dt.Second.ToString();

            return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
        }
    }
}
