using System;
using Microsoft.SPOT;

namespace GPS_GSM_Unit
{
    public class GPRSPort
    {
        public GPRSPort(string portName, int defIndex)
        {
            this.PortName = portName;
            this.DefIndex = defIndex;
        }
        public string PortName { get; set; }
        public int DefIndex { get; set; }
    }
}
