using System;
using Microsoft.SPOT;

namespace GPS_GSM_Unit
{
    public class IOData
    {
        public IOData(string temp_1, string temp_2, string on_off_1, string analog1)
        {
            this.Temp1 = temp_1;
            this.Temp2 = temp_2;
            this.OnOff1 = on_off_1;
            this.Analog1 = analog1;
        }

        public string Temp1 { get; set; }
        public string Temp2 { get; set; }
        public string OnOff1 { get; set; }
        public string Analog1 { get; set; }        
    }
}
