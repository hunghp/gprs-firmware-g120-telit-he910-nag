﻿using System;
using System.Text;
using System.IO;
using System.Collections;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.IO;
using System.IO.Ports;
using System.Threading;
using GHI.Pins;
using GHI.Utilities;
using GHI.Processor;

using GHI.IO;
using GHI.IO.Storage;

//Code for G120 chipset
namespace GPS_GSM_Unit
{            
    public class Program
    {
        //Hardware info
        static int _HW_VER = 17000;  //go together with _HW_SERIAL
        static int _HW_SERIAL = 17;  //go together with _HW_VER, unique incremental for each unit
        static int _FW_VER = 0;                

        //Hardware variables
        //static SerialPort _spCOM1 = new SerialPort("COM1", 19200, Parity.None, 8, StopBits.One);
        static SerialPort _sp3GModule = new SerialPort("COM2", 115200, Parity.None, 8, StopBits.One);
        static SerialPort _spCOM3 = new SerialPort("COM3", 9600, Parity.None, 8, StopBits.One);        

        static OutputPort _reset3GModule = new OutputPort(GHI.Pins.G120.P3_26, true);
        static OutputPort _resetXBEE = new OutputPort(GHI.Pins.G120.P0_13, true);

        static OutputPort _tempPin_1 = new OutputPort(GHI.Pins.G120.P0_27, false);
        static OutputPort _tempPin_2 = new OutputPort(GHI.Pins.G120.P0_28, false); 
        static DS18B20 _ds18B20_1 = new DS18B20(_tempPin_1);
        static DS18B20 _ds18B20_2 = new DS18B20(_tempPin_2);

        static InputPort _onoff_1 = new InputPort(GHI.Pins.G120.P0_26, true, Port.ResistorMode.PullUp);       
        static AnalogInput _analog_0_5V_1 = new AnalogInput((Cpu.AnalogChannel)Cpu.AnalogChannel.ANALOG_5);  //P1.31   

        //Interrupt pin to send a complete package at anytime
        static InterruptPort _send_package_interrupt = new InterruptPort(GHI.Pins.G120.P2_2, true, Port.ResistorMode.PullUp, Port.InterruptMode.InterruptEdgeLow);
                      
        //LED////////////////////////
        //  _statusLED:
        //      1. Solid On, SD card mount failed or load setting file failed.
        //      2. Keep Blinking non-stop every 200ms, load sensor from database failed or register wired and wireless sensor configuration failed or write sensors configuration to the database failed.  
        //      3. Keep Blinking non-stop every 1000ms, enter AT command mode for XBEE module failed
        //      4. Blink 3 times and stay off, load setting file and sensors from database OK.        
        //      5. Blink 3 times after load setting file OK, intialized and auto config/program XBEE successfully.
        //              
        // _gpsOnlineLED:
        //      1. Solid On, gps unit is power and receiving data.
        //      2. Solid On and blink 3 times every 3 seconds, gps unit is locked and have valid location data
        //
        // _3gOnlineLED:
        //      1. Solid On, unit is powered and ready to send and receive.
        //             
        // _transmissionLED:
        //      1. Stay On while the system is transmitting or receiving data.                
        //      2. Blink 5 times, signal that an interrupt of requesting the system to transmit out a package
        static OutputPort _3gOnlineLED = new OutputPort(GHI.Pins.G120.P3_24, false);
        static OutputPort _gpsOnlineLED = new OutputPort(GHI.Pins.G120.P0_1, false);
        static OutputPort _transmissionLED = new OutputPort(GHI.Pins.G120.P3_25, false);
        static OutputPort _statusLED = new OutputPort(GHI.Pins.G120.P0_0, false);       


        //System data objects and variables
        static GPSData _GPSDataObject = new GPSData(new DateTime(1980, 5, 13, 0, 0, 0), "", "", "", "", "");       
        static bool _3GOnline = false;
        static bool _GPSOnline = false;
        static bool _IsRTCSet = false;             
        static int _LogFileIndex = 0;
        static int _DownloadLogFileIndex = 0;
        static int MAX_LOG_FILE_SIZE = 800;  //in bytes
        static bool _GPSLocked = false;       

        //System Settings variables
        static int _PROCESS_GPS_DATA_INTERVAL = 5;  //in seconds
        static int _TRANSMIT_INTERVAL = 5;  //default value 5 minutes
        static string _DEVICE_NAME = System.String.Empty;               
        static string _SIMPhoneNo = System.String.Empty;
        static string _ip = System.String.Empty;
        static string _port = System.String.Empty;        
        static string _apn_name = System.String.Empty;
        static string _apn_username = System.String.Empty;
        static string _apn_password = System.String.Empty;
        static int _wireless_sensor_offline_threshold = 15;  //Wireless Sensor offline in minute.  Default 15 minutes
        static string _epoch_time_stamp = System.String.Empty;
        static string _setting_id = System.String.Empty;
        static string _sensors_definition = System.String.Empty;        
        
        //Threads
        static Thread _gpsThread = new Thread(new ThreadStart(GPSThread));
        static Thread _doWork = new Thread(new ThreadStart(DoMainWork));
        static Thread _pointSixThread = new Thread(new ThreadStart(MonitorPointSixData));   //Thread that responsible for Point Six package download and parsing
       
        //XOR passphrase
        static byte[] _xorPass = new byte[] { 115, 99, 105, 101, 110, 116, 105, 115, 116, 115, 32, 105, 110, 118, 101, 115, 116, 105, 103, 97, 116, 101, 32, 116, 
                                      104, 97, 116, 32, 119, 104, 105, 99, 104, 32, 108, 114, 101, 97, 100, 121, 32, 105, 115, 59, 32, 101, 110, 103, 105, 
                                      110, 101, 101, 114, 115, 32, 99, 114, 101, 97, 116, 101, 32, 116, 104, 97, 116, 32, 119, 104, 105, 99, 104, 32, 104, 
                                      97, 115, 32, 110, 101, 118, 101, 114, 32, 98, 101, 101, 110, 32, 45, 32, 97, 108, 98, 101, 114, 116, 32, 101, 105, 110, 
                                      115, 116, 101, 105, 110 };        
        
       
        //Static sd card instance
        static SDCard _sdCard;
        static SDStorage _sdStor;        

        //Binary programming pin for xbee 900 module
        static OutputPort _xbee_cmd_pin = new OutputPort(GHI.Pins.G120.P1_17, false);


        /////////////////////////////////////////////////////
        //  Point Six Sensor Global variables              //
        /////////////////////////////////////////////////////
        static int _PointSixSensorBytesLength = 75;

        //Store newly received point six package
        static byte[] t_PointSixPackage = new byte[75];
        static bool _NewPointSixPackageReceived = false;

        static DateTime _timeAdjustment;
        static DateTime _sensorDownloadDateTime;
        static string _REQ_DATA_CMD = "C33C0020";

        static string[] _byteStrArray = new string[75];
        static bool _stopWaiting = false;
        static byte[] _sendPacket = null;
        static int _ps_download_count_1 = 0;
        static int _ps_download_count_2 = 0;
        static int _oldestBlock = -1;               //-2: Need to get oldest block record but failed, -1: Doesn't need to get oldest block record, >-1: Block record acquired successfully.

        static ArrayList _SensorList = new ArrayList();
        static ArrayList _IncompleteDownloadList = new ArrayList();
        static SensorRecord _parsedSensorRecord = null;

        static int UpdateRTCCounter = 0;

        public static void Main()
        {
            //RealTimeClock.SetDateTime(new DateTime(2016, 1, 29, 9, 40, 0));
            //Debug.Print("RTC Set.");
            //Debug.Print(RealTimeClock.GetDateTime().ToString());
            //Utility.SetLocalTime(RealTimeClock.GetDateTime());

            ////Detect system restart by WatchDog. Normally, you can read this flag ***ONLY ONCE*** on power up
            //if (GHI.Processor.Watchdog.LastResetCause == GHI.Processor.Watchdog.ResetCause.Watchdog)
            //{
            //    SpecialGlobalVar._SystemRebootByWatchDog = true;
            //}
            //else
            //{
            //    SpecialGlobalVar._SystemRebootNotByWatchDog = true;
            //}

            //Enable WATCHDOG TIMER
            GHI.Processor.Watchdog.Enable(32000);
            ResetWatchDog();

            HUtility util = new HUtility();

            try 
            { 
                _sdCard = new SDCard();
            }
            catch (Exception ex)
            {
                Debug.Print("From [Main], initialize an instance of sd card failed: " + ex.ToString());

                //System needs restart, set watchdog timer flag            
                SpecialGlobalVar._SystemNeedReboot = true;

                _statusLED.Write(true);

                //Stay here until system reboot by the Watch Dog
                while (true)
                {
                    //Wait until system reset by watch dog
                }
            }
            
            //Mount the sd card and create an instance of it and assign to a static variable
            try
            {
                bool fs_ready = false;
                _sdCard.Mount();

                RemovableMedia.Insert += (a, b) =>
                {
                    fs_ready = true;
                };
               
                while (!fs_ready)
                {
                    Thread.Sleep(50);
                }

                _sdStor = new SDStorage(_sdCard);
            }
            catch (Exception ex)
            {
                Debug.Print("From [Main], mount sd card failed: " + ex.ToString());

                //System needs restart, set watchdog timer flag            
                SpecialGlobalVar._SystemNeedReboot = true;

                _statusLED.Write(true);

                //Stay here until system reboot by the Watch Dog
                while (true) 
                {
                    //Wait until system reset by watch dog
                }
            }

            //Load settings from sd card           
            LoadSettingsFromSDCard();            

            //Load flash object using extended weak reference object
            //Load flag to set if RTC is already set
            ExtendedWeakReference ewrObject = ExtendedWeakReference.RecoverOrCreate(typeof(bool), 0, ExtendedWeakReference.c_SurvivePowerdown);
            if (ewrObject.Target != null)
            {
                _IsRTCSet = (bool)ewrObject.Target; 
            }
            ExtendedWeakReference.FlushAll();
           
            ResetWatchDog();

            //Set system local time if RTC has been set using Cellular
            if (_IsRTCSet == true)
                Utility.SetLocalTime(RealTimeClock.GetDateTime());
          
            //Load sensors from database
            DB db = new DB();
            ArrayList tempSensorList = new ArrayList();
            SensorRecord tempSensorRecord = new SensorRecord(null, -1, DateTime.Now, -1, -1, null, -1, -1, -1, -1, -1, -1, -1);
            GPRSPort tempGPRSPort = new GPRSPort(null, -1);
            if (db.OpenDB() == true)
            {
                tempSensorList = db.GetSensorList();               
                                
                if (tempSensorList != null) 
                {
                    if (tempSensorList.Count > 0)
                    {
                        int defIndexCount = 1;
                        while (defIndexCount <= tempSensorList.Count)
                        {
                            foreach (var sensor in tempSensorList)
                            {
                                if (sensor is SensorRecord)
                                {
                                    //check definition index
                                    tempSensorRecord = sensor as SensorRecord;
                                    if (tempSensorRecord.DefIndex == defIndexCount)
                                    {
                                        _SensorList.Add(tempSensorRecord);
                                        defIndexCount++;
                                        break;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                else if (sensor is GPRSPort)
                                {
                                    //check definition index
                                    tempGPRSPort = sensor as GPRSPort;
                                    if (tempGPRSPort.DefIndex == defIndexCount)
                                    {
                                        _SensorList.Add(tempGPRSPort);
                                        defIndexCount++;
                                        break;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                else
                                {
                                    continue;
                                }
                            }
                        }
                    }
                }
                else   //The database return null, no sensor in the table
                {
                    //We need to register the sensors into the database from the _sensors_definition variable which was read from the settings.txt file.
                    if (_sensors_definition != null || _sensors_definition == String.Empty)
                    {
                        RegisterWiredAndWirelessSensorsConfiguration(_sensors_definition);
                    }
                    else
                    {
                        //No definition either, we need to blink error
                        db.CloseDB();
                        SpecialGlobalVar._SystemNeedReboot = true;
                        while (true)
                        {
                            util.BlinkLED(_statusLED, 2, 200, LED_Blink_Type.OFF_ON);
                        }
                    }
                }                               
            }                             
            
                       
            //Load incomplete download list if it exist
            LoadIncompleteDownloadList();

            //Indicate finish loading settings from SD Card and sensors from Database
            util.BlinkLED(_statusLED, 3, 300, LED_Blink_Type.ON_OFF);

            ResetWatchDog();

            ///////////////////test code/////////////////////////
            //SensorRecord srTest = null;
            //Debug.Print("---------------------------------REGISTERED SENSORS LIST---------------------------");
            //foreach (var sensor in _SensorList)
            //{
            //    if (sensor is SensorRecord)
            //    {
            //        srTest = sensor as SensorRecord;
            //        Debug.Print(srTest.ToString() + "\n");
            //    }
            //}

            //Debug.Print("---------------------------------END LISTS-----------------------------------------");



            //IncompleteDownloadRecord ir = null;
            //Debug.Print("---------------------------------INCOMPLETE SENSORS LIST---------------------------");
            //foreach (var record in _IncompleteDownloadList)
            //{
            //    ir = record as IncompleteDownloadRecord;
            //    Debug.Print(ir.ToString() + "\n");
            //}

            //Debug.Print("---------------------------------END INCOMPLETE LIST-------------------------------");

            /////////////////////////////////////////////////////


            //Initalize COM3 serial port for XBee 900 Transceiver
            //Open COM port
            if (util.InitializeComPort(_spCOM3) == true)
            {                
                //Wait 1 second for serial port to initialize
                Thread.Sleep(1000);

                //Program the XBEE module with important settings
                int auto_prog_state = 0;
                bool auto_prog_error = false;
                while (true)
                {
                    //Check to see if auto program status is causing problem
                    if (auto_prog_error == true)
                    {
                        //Stay here forever until watchdog restart the system, XBEE problem, can't auto program some settings
                        while (true)
                        {
                            util.BlinkLED(_statusLED, 1, 1000, LED_Blink_Type.ON_OFF);
                        }
                    }

                    //Done auto programming XBEE module
                    if (auto_prog_state == 8)
                        break;

                    switch (auto_prog_state)
                    {
                        case 0:  //Enter AT command mode, try 2 times
                            for (int i = 0; i < 2; i++)
                            {
                                if (util.SendCmdToUART(_spCOM3, "+++", "OK", 3000, false) == false && i == 1)
                                {
                                    auto_prog_error = true;
                                    break;
                                }
                            }                            
                            auto_prog_state = 1;
                            break;
                        case 1:  //Set baud rate to 9600
                            if (util.SendCmdToUART(_spCOM3, "ATBD3,WR", "OK", 500, true) == false)
                            {
                                auto_prog_error = true;
                                break;
                            }                                
                            auto_prog_state = 2;
                            break;
                        case 2:  //Set destination address to 0x0505
                            if (util.SendCmdToUART(_spCOM3, "ATDT0505,WR", "OK", 500, true) == false)
                            {
                                auto_prog_error = true;
                                break;
                            }
                            auto_prog_state = 3;
                            break;
                        case 3:  //Change hoping channel to 3
                            if (util.SendCmdToUART(_spCOM3, "ATHP3,WR", "OK", 500, true) == false)
                            {
                                auto_prog_error = true;
                                break;
                            }
                            auto_prog_state = 4;
                            break;
                        case 4:  //Set Address Mask to 0x0000
                            if (util.SendCmdToUART(_spCOM3, "ATMK0000,WR", "OK", 500, true) == false)
                            {
                                auto_prog_error = true;
                                break;
                            }
                            auto_prog_state = 5;
                            break;
                        case 5:  //Set Binary programming mode
                            if (util.SendCmdToUART(_spCOM3, "ATRT1,WR", "OK", 500, true) == false)
                            {
                                auto_prog_error = true;
                                break;
                            }
                            auto_prog_state = 6;
                            break;
                        case 6:  //Set Power Level to 4 (max)
                            if (util.SendCmdToUART(_spCOM3, "ATPL4,WR", "OK", 500, true) == false)
                            {
                                auto_prog_error = true;
                                break;
                            }
                            auto_prog_state = 7;
                            break;
                        case 7:  //Exit AT command mode
                            util.SendCmdToUART(_spCOM3, "ATCN", "OK", 500, true);                            
                            auto_prog_state = 8;
                            break;                        
                    }
                }

                //After auto program XBEE, now we can set data event for COM3                                                             
                _spCOM3.DataReceived += _spCOM3_DataReceived;

                //Indicate intialized and auto config/program XBEE successfuly
                util.BlinkLED(_statusLED, 3, 300, LED_Blink_Type.ON_OFF);
            }
            else
            {
                //reset the system
                SpecialGlobalVar._SystemNeedReboot = true;
                Thread.Sleep(Timeout.Infinite);
            }

            ResetWatchDog();

            //Set up interrupt pin            
            _send_package_interrupt.OnInterrupt += _send_package_interrupt_OnInterrupt;            
        
            ////Initialize COM1
            //if (util.InitializeComPort(_spCOM1) != true)
            //{
            //    //reset the system
            //    SpecialGlobalVar._SystemNeedReboot = true;
            //    Thread.Sleep(Timeout.Infinite);
            //}

            //Initialize serial port for 3G module
            if (util.InitializeComPort(_sp3GModule) != true)
            {
                //reset the system
                SpecialGlobalVar._SystemNeedReboot = true;
                Thread.Sleep(Timeout.Infinite);
            }

            //Hard reset 3G module with basic mandatory setup
            util.Reset3GModuleWithMandatoryBasicSetup(_sp3GModule, _reset3GModule);
                        
            //Activate GPS on the module
            InitializeGPSUnit();

            ResetWatchDog();

            //Start gps thread, this monitor GPS lock or not by perodically check GPS reading from the module
            if (!_gpsThread.IsAlive)
                _gpsThread.Start();

            //Start point six monitoring thread, let XBEE 900 module start receiving data from Point Six sensors
            if (!_pointSixThread.IsAlive)
                _pointSixThread.Start();

            //Start main work Thread
            if (!_doWork.IsAlive)
                _doWork.Start();  

            //Initialize GSM, this is last because we don't know how long before we get cellular network and register our module         
            InitializeCellularNetwork();
                                                         
            Thread.Sleep(Timeout.Infinite);            
        }

        //Send out one complete package when interrupt occurred
        static void _send_package_interrupt_OnInterrupt(uint data1, uint data2, DateTime time)
        {
            if (SpecialGlobalVar._3GModuleSerialPortBusy == true)  //the port is busy
            {
                _send_package_interrupt.ClearInterrupt();
                return;
            }

            SpecialGlobalVar._3GModuleSerialPortBusy = true;

            //Start transmitting one package       
            HUtility util = new HUtility();
                 
                                    
            //Indicate start processing data and preparing to transmit when interrupt happen
            util.BlinkLED(_transmissionLED, 5, 100, LED_Blink_Type.ON_OFF);

            //Trasmit one complete data package through TCP/IP
            TransmitDeviceDataPackage();

            //Update registered sensors and their data to the database
            UpdateSensorRecordToDatabase();
                      
            //After finished transmitting, reset flag
            SpecialGlobalVar._3GModuleSerialPortBusy = false;

            _send_package_interrupt.ClearInterrupt();            
        }

        //COM3 DataReceived event, listening for serial data from XBee 900 Transceiver
        static void _spCOM3_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int readByte = 0;            

            while (_spCOM3.BytesToRead > 0)
            {
                //Look for the package header "C33C"
                if (SpecialGlobalVar._COM3TotalBytesRead == 0)
                {
                    readByte = _spCOM3.ReadByte();  //read first byte
                    if (readByte != 195)
                    {
                        continue;
                    }
                    else
                    {
                        SpecialGlobalVar._XBEE900SerialBuffer[0] = (byte)readByte;
                    }

                    readByte = _spCOM3.ReadByte(); //read second byte
                    if (readByte != 60)
                    {
                        continue;
                    }
                    else
                    {
                        SpecialGlobalVar._XBEE900SerialBuffer[1] = (byte)readByte;
                    }

                    readByte = _spCOM3.ReadByte(); //read third byte
                    if (readByte != 0)
                    {
                        continue;
                    }
                    else
                    {
                        SpecialGlobalVar._XBEE900SerialBuffer[2] = (byte)readByte;
                    }

                    SpecialGlobalVar._COM3TotalBytesRead = 3;
                }

                //package header found start reading byte to buffer if buffer is not full
                if (SpecialGlobalVar._COM3TotalBytesRead < _PointSixSensorBytesLength)
                {
                    SpecialGlobalVar._XBEE900SerialBuffer[SpecialGlobalVar._COM3TotalBytesRead] = (byte)_spCOM3.ReadByte();
                    SpecialGlobalVar._COM3TotalBytesRead++;
                }
                else //buffer is full, keep reading until no more bytes to read so the serial data event will fire again
                {
                    readByte = _spCOM3.ReadByte();
                }

            }// end while loop

            if (SpecialGlobalVar._COM3TotalBytesRead >= _PointSixSensorBytesLength)
            {
                SpecialGlobalVar._COM3TotalBytesRead = 0;  //reset variable
                _NewPointSixPackageReceived = true;
                
            }                        
        }

        /////////////
        // Threads //
        /////////////

        #region [Threads Processes]       

        //GPS processing thread method
        static void GPSThread()
        {               
            while (true)
            {
                Thread.Sleep(_PROCESS_GPS_DATA_INTERVAL * 1000);

                ResetWatchDog();                     

                //if serial port is busy, just continue
                if (SpecialGlobalVar._3GModuleSerialPortBusy == true)
                {
                    SpecialGlobalVar._GPSThreadIsOnHold = true;
                    continue;
                }

                SpecialGlobalVar._GPSThreadIsOnHold = false;

                HUtility util = new HUtility();
                               
                //set flag
                SpecialGlobalVar._3GModuleSerialPortBusy = true;

                //Get GPS Coordinate
                GPSData gpsData = util.GetAcquiredGPSCoordinate(_sp3GModule);

                //reset flag
                SpecialGlobalVar._3GModuleSerialPortBusy = false;

                //check if RTC is set, if not check to see if cell network is available 
                //If so, set the RTC using network date time
                if (_3GOnline == true && _IsRTCSet == false)
                {
                    //set flag
                    SpecialGlobalVar._3GModuleSerialPortBusy = true;

                    //Set the RTC using network time here
                    int[] networkDT = util.GetCellularNetworkDateTime(_sp3GModule);

                    if (networkDT != null && networkDT.Length == 6)
                    {
                        RealTimeClock.SetDateTime(new DateTime(networkDT[0], networkDT[1], networkDT[2], networkDT[3], networkDT[4], networkDT[5]));
                        Utility.SetLocalTime(RealTimeClock.GetDateTime());

                        //Write RTC flag to flash memory using extended weak reference
                        ExtendedWeakReference ewrObject = ExtendedWeakReference.RecoverOrCreate(typeof(bool), 0, ExtendedWeakReference.c_SurvivePowerdown);
                        ewrObject.Target = true;
                        ExtendedWeakReference.FlushAll();

                        _IsRTCSet = true;
                    }

                    //reset flag
                    SpecialGlobalVar._3GModuleSerialPortBusy = false;
                }

                //increment updateRTCCounter variable
                UpdateRTCCounter++;

                //Check to see if it is 15 minutes yet, counter should be 180 or greater
                if (UpdateRTCCounter > 180)
                {
                    UpdateRTCCounter = 0; //reset counter

                    //Update RTC if it is out of sync for more than 1 minutes from cellular date time    
                    //Check this every 15 minutes
                    try
                    {
                        if (_3GOnline == true)
                        {
                            //set flag
                            SpecialGlobalVar._3GModuleSerialPortBusy = true;

                            //get network DT 
                            int[] networkDT = util.GetCellularNetworkDateTime(_sp3GModule);

                            if (networkDT != null && networkDT.Length == 6)
                            {
                                DateTime newNetworkDT = new DateTime(networkDT[0], networkDT[1], networkDT[2], networkDT[3], networkDT[4], networkDT[5]);
                                int seconds_diff = (int)((newNetworkDT.Subtract(DateTime.Now).Ticks / TimeSpan.TicksPerSecond));
                                seconds_diff = System.Math.Abs(seconds_diff);
                                if (seconds_diff >= 60)
                                {
                                    RealTimeClock.SetDateTime(newNetworkDT);

                                    //Set system local time                                
                                    Utility.SetLocalTime(RealTimeClock.GetDateTime());

                                    //Write RTC flag to flash memory using extended weak reference
                                    ExtendedWeakReference ewrObject = ExtendedWeakReference.RecoverOrCreate(typeof(bool), 0, ExtendedWeakReference.c_SurvivePowerdown);
                                    ewrObject.Target = true;
                                    ExtendedWeakReference.FlushAll();

                                    _IsRTCSet = true;
                                }
                            }

                            //reset flag
                            SpecialGlobalVar._3GModuleSerialPortBusy = false;
                        }
                    }
                    catch
                    {
                        //Exception happens, system need reboot/reset
                        SpecialGlobalVar._SystemNeedReboot = true;
                        Thread.Sleep(Timeout.Infinite);  //wait for watchdog to reset system
                    }
                }

                //If GPS is not lock, then no data, set all parameters to default to indicate no GPS lock
                if (gpsData == null)
                {
                    _GPSLocked = false;                    
                    _GPSDataObject.UTCDateTime = new DateTime(1980, 5, 13, 0, 0, 0);
                    _GPSDataObject.Longitude = "";
                    _GPSDataObject.Latitude = "";
                    _GPSDataObject.Altitude_In_Meter = "";
                    _GPSDataObject.Speed_KM_Per_Hour = "";
                    _GPSDataObject.Number_Of_Sat_In_Use = "";

                    continue;
                }                                
                               
                //Update latest gps coordinate data to static object _GPSDataObject
                lock (_GPSDataObject)
                {
                    _GPSDataObject.UTCDateTime = DateTime.Now;
                    _GPSDataObject.Longitude = gpsData.Longitude;
                    _GPSDataObject.Latitude = gpsData.Latitude;
                    _GPSDataObject.Altitude_In_Meter = gpsData.Altitude_In_Meter;
                    _GPSDataObject.Speed_KM_Per_Hour = gpsData.Speed_KM_Per_Hour;
                    _GPSDataObject.Number_Of_Sat_In_Use = gpsData.Number_Of_Sat_In_Use;                    
                    
                    _GPSLocked = true;

                    util.BlinkLED(_gpsOnlineLED, 3, 200, LED_Blink_Type.OFF_ON);
                }
                                                                              
            }                 
        }
        
        //Do main work thread
        static void DoMainWork()
        {            
            HUtility util = new HUtility();                                                                  
            while (true)
            {
                //Save incomplete download list to file
                SaveIncompleteListToFile();

                
                //retries 5 times to see if the serial port is not busy to start transmit data
                for (int i = 0; i <= 5; i++)
                {
                    //serial port is not busy, OK to start transmitting
                    if (SpecialGlobalVar._3GModuleSerialPortBusy == false)
                    {
                        //Set flag 
                        SpecialGlobalVar._3GModuleSerialPortBusy = true;

                        //Trasmit one complete data package through TCP/IP
                        TransmitDeviceDataPackage();

                        //After transmit at regular interval, update the database
                        UpdateSensorRecordToDatabase();

                        //Reset flag 
                        SpecialGlobalVar._3GModuleSerialPortBusy = false;

                        break;
                    }
                    else
                    {                        
                        Thread.Sleep(500);
                    }
                }
                
                //check to see if connect to HUB failed retries exceed certain amount of number, then try initialize Cellular again
                if (SpecialGlobalVar._TCPConnectionRetries >= 3)
                {
                    SpecialGlobalVar._TCPConnectionRetries = 0;
                    InitializeCellularNetwork();
                }

                Thread.Sleep(_TRANSMIT_INTERVAL * 60000);
            }
        }

        //Monitor PointSix Wireless Sensors Process
        static void MonitorPointSixData()
        {
            string beaconPackage = "";
            while (true)
            {
                //No new package receive, continue
                if (!_NewPointSixPackageReceived)
                    continue;

                _NewPointSixPackageReceived = false;    //reset flag
                //SpecialGlobalVar._XBEE900SerialBuffer.CopyTo(_PointSixPackage, 0);    //copy to another arraylist

                //Check to see if it is a beacon package, ignore and continue
                if (SpecialGlobalVar._XBEE900SerialBuffer[2] != 0 && SpecialGlobalVar._XBEE900SerialBuffer[3] != 02)      //beacon package 0x0022
                {
                    //Clean out garbage in the serial port
                    _spCOM3.Flush();
                    _spCOM3.DiscardInBuffer();
                    _spCOM3.DiscardOutBuffer();
                    continue;
                }

                //Store the beacon package
                beaconPackage = HTools.ByteArr.ToHex(SpecialGlobalVar._XBEE900SerialBuffer);

                //Parse the package                
                _parsedSensorRecord = ParsePackage(SpecialGlobalVar._XBEE900SerialBuffer);

                //Unable to parse package, ignore and continue                
                if (_parsedSensorRecord == null)
                {
                    continue;
                }

                //Debug.Print(_parsedSensorRecord.ToString());

                //Find the sensor, if it's not registered, continue                
                int sensorRecordIndex = -1;
                for (int i = 0; i < _SensorList.Count; i++)
                {
                    //Check to see if it's a wireless sensor object
                    if (_SensorList[i] is SensorRecord)
                    {
                        SensorRecord sr = _SensorList[i] as SensorRecord;
                        if (String.Compare(sr.SensorID, _parsedSensorRecord.SensorID) == 0)
                        {
                            sensorRecordIndex = i;
                            break;
                        }
                    }
                }

                if (sensorRecordIndex < 0)  //not found
                {
                    continue;
                }

                //Get the found sensor record from the registered list
                SensorRecord registeredSensor = _SensorList[sensorRecordIndex] as SensorRecord;

                //If it is a newly added sensor, no need to check to see if sensor is delay or not, no need to download anything, just update the record
                if (registeredSensor.SensorClock < 0)
                {
                    Send_PointSix_Sensor_ACK_CMD(_parsedSensorRecord.SensorID, 3);  //tell the sensor to sleep
                    UpdateSensorRecord(_parsedSensorRecord, registeredSensor);      //update sensor data               
                    continue;
                }


                DateTime startDT = DateTime.Now;
                DateTime endDT = DateTime.Now;
                int[] memoryBlockList = null;
                //Check to see if sensor is delay
                if (IsSensorDelay(registeredSensor, _parsedSensorRecord, _wireless_sensor_offline_threshold))
                {                                       
                    startDT = registeredSensor.TimeStamp;
                    endDT = _parsedSensorRecord.TimeStamp;

                    //Get memory block list      
                    memoryBlockList = GetMemoryListForDownload(_parsedSensorRecord, registeredSensor, _oldestBlock);
                }
                else if (_IncompleteDownloadList.Count > 0)  //Check to see if sensor is on the incomplete download list
                {
                    bool found = false;
                    //Find the sensor in the list
                    for (int i = 0; i < _IncompleteDownloadList.Count; i++)
                    {
                        IncompleteDownloadRecord irec = _IncompleteDownloadList[i] as IncompleteDownloadRecord;
                        if (String.Compare(irec.SensorID, _parsedSensorRecord.SensorID) == 0)
                        {
                            startDT = irec.StartDate;
                            endDT = irec.EndDate;

                            //Memory rolled over, generate 2 sequences, from sr.BlockIndex to FF = 255 block, and from 0 to current block index
                            if (irec.StartMemBlock > irec.EndMemBlock)
                            {
                                ArrayList memArray1 = GetMemoryList(irec.StartMemBlock, 255);
                                ArrayList memArray2 = GetMemoryList(0, irec.EndMemBlock);
                                Array memList = Array.CreateInstance(typeof(int), memArray1.Count + memArray2.Count);
                                memArray1.CopyTo(memList, 0);
                                memArray2.CopyTo(memList, memArray1.Count);
                                memoryBlockList = (int[])memList;
                            }
                            else //Same block index or normal case            
                            {
                                ArrayList memArray = GetMemoryList(irec.StartMemBlock, irec.EndMemBlock);
                                Array memList = Array.CreateInstance(typeof(int), memArray.Count);
                                memArray.CopyTo(memList, 0);
                                memoryBlockList = (int[])memList;
                            }

                            //Delete this record from the list
                            _IncompleteDownloadList.RemoveAt(i);
                            //Save the new incomplete download list to file
                            SaveIncompleteListToFile();

                            found = true;
                            break;
                        }
                    }

                    if (found == false)
                        continue;
                }
                else
                {
                    Send_PointSix_Sensor_ACK_CMD(_parsedSensorRecord.SensorID, 3);  //tell the sensor to sleep
                    UpdateSensorRecord(_parsedSensorRecord, registeredSensor);      //update sensor data                                             
                    continue;
                }


                //Check to see if sensor clock of the current package is less than the recorded one,
                //we need to request the oldest log and download from there until recent received block                                
                if (_parsedSensorRecord.SensorClock < registeredSensor.SensorClock)
                {
                    //Need to get oldest record
                    _oldestBlock = GetOldestBlock(_parsedSensorRecord.SensorID.Trim());

                    //Get memory block list again if oldest block is required                
                    memoryBlockList = GetMemoryListForDownload(_parsedSensorRecord, registeredSensor, _oldestBlock);
                }

                /////////////////////////////////////////////////////////////
                //  Start downloading memory blocks from sensor procedure  //
                /////////////////////////////////////////////////////////////                           

                //Change xbee destination address to request log download for the sensor that just came in.
                _xbee_cmd_pin.Write(true);      // Pull pin 16 high
                Thread.Sleep(1);
                string xbee_bin_cmd = "00" + _parsedSensorRecord.SensorID.Substring(6, 2) + _parsedSensorRecord.SensorID.Substring(4, 2) + "08";    //Set Destination Address to the sensor address
                byte[] xbee_bin_cmd_bytes = HTools.Hex.ToBytes(xbee_bin_cmd);
                _spCOM3.Write(xbee_bin_cmd_bytes, 0, xbee_bin_cmd_bytes.Length);
                Thread.Sleep(1);

                //Set Address Mask to 0xFFFF, this will allow the sensor to communicate with the host without the host seeing packages from other sensors, 
                //this should eliminate interference and allow the host to collect all memory blocks in one session
                xbee_bin_cmd = "12FFFF08";
                xbee_bin_cmd_bytes = HTools.Hex.ToBytes(xbee_bin_cmd);
                _spCOM3.Write(xbee_bin_cmd_bytes, 0, xbee_bin_cmd_bytes.Length);
                Thread.Sleep(1);
                _xbee_cmd_pin.Write(false); // Pull pin 16 low               

                //Download history records from sensor
                DownloadHistoryRecordsAndSendItToHub(_parsedSensorRecord, memoryBlockList, startDT, endDT);

                //Update the sensor after trying to download to the most recent date time
                UpdateSensorRecord(_parsedSensorRecord, registeredSensor);

                //Write incomplete download list to file if there are any
                SaveIncompleteListToFile();

                //Update sensors to database
                UpdateSensorRecordToDatabase();

                
                //Set Address Mask back to 0x0000, this will allow all sensor to communicate with the host again, 
                _xbee_cmd_pin.Write(true);      // Pull pin 16 high
                Thread.Sleep(1);
                xbee_bin_cmd = "12000008";      // Set Address Mask to 0x0000
                xbee_bin_cmd_bytes = HTools.Hex.ToBytes(xbee_bin_cmd);
                _spCOM3.Write(xbee_bin_cmd_bytes, 0, xbee_bin_cmd_bytes.Length);
                Thread.Sleep(1);
                _xbee_cmd_pin.Write(false);     // Pull pin 16 low

            }

        }
       
        #endregion


        ////////////////////
        // Helper Methods //
        ////////////////////

        #region [Helper Methods]
       
        /// <summary>
        /// Reset the watch dog timer
        /// </summary>
        public static void ResetWatchDog()
        {
            //Reset WatchDog Timer here
            if (SpecialGlobalVar._SystemNeedReboot == false)
            {
                //reset the watch dog, stop it from resetting the system.
                GHI.Processor.Watchdog.ResetCounter();
            }
        }
                
        //Initializing GSM unit
        public static void InitializeCellularNetwork()
        {   
            //wait until the serial port is free
            while(SpecialGlobalVar._3GModuleSerialPortBusy)
            {
                //wait until it is free
            }
          
            HUtility util = new HUtility();
            int state = 1; //start at state 1
            string networkStatus = null;
            _3gOnlineLED.Write(false);
            _3GOnline = false;
            bool PDPContextActivated = false;

            //Loop until Cellular network is initialize and registered
            while (true)
            {                                             
                switch(state)
                {
                    case 0:
                        //need system reset
                        SpecialGlobalVar._SystemNeedReboot = true;
                        //Sleep until watchdog reset the system, 30 seconds watchdog, sleep forever...
                        Thread.Sleep(Timeout.Infinite);
                        break;

                    case 1:
                        //set flag, to stop GPS thread from interrupting if it is running
                        SpecialGlobalVar._3GModuleSerialPortBusy = true;

                        //set PDP context
                        if (util.SetPDPContext(_sp3GModule, _apn_name) == true)
                        {
                            state = 2;
                            break;
                        }
                        else
                        {
                            //Do hard reset with basic mandatory setup, then initialize GPS, then go back to state = 1
                            //This basically repeat the same initialization step when the system is reset or reboot
                            util.Reset3GModuleWithMandatoryBasicSetup(_sp3GModule, _reset3GModule);
                            InitializeGPSUnit();
                            state = 1;
                            break;
                        }                        
                        
                    case 2:
                        //Configuring the embedded TCP/IP stack (set timeout, package size etc...)
                        if (util.ConfigureTCPStackParameters(_sp3GModule) == true)
                        {
                            //resset flag, this allows gps thread to start getting coordinate and DoMainWork thread to do its work
                            SpecialGlobalVar._3GModuleSerialPortBusy = false;
                            state = 3;
                            break;
                        }
                        else
                        {
                            //Do hard reset with basic mandatory setup, then initialize GPS, then go back to state = 1
                            //This basically repeat the same initialization step when the system is reset or reboot
                            util.Reset3GModuleWithMandatoryBasicSetup(_sp3GModule, _reset3GModule);
                            InitializeGPSUnit();
                            state = 1;
                            break;
                        }
                    
                    case 3:
                        //is module registered to the network
                        if (util.Is3GModuleRegisteredToNetwork(_sp3GModule, out networkStatus) == true)
                        {
                            state = 4;
                            break;
                        }
                        else
                        {                           
                            if (String.Compare(networkStatus, "REGISTRATION_DENIED") == 0 || String.Compare(networkStatus, "UNKNOWN") == 0 ||
                                     String.Compare(networkStatus, "UNKNOWN_RESPONSE_FROM_MODULE") == 0)
                            {
                                //Do hard reset with basic mandatory setup, then initialize GPS, then go back to state = 1
                                //This basically repeat the same initialization step when the system is reset or reboot
                                util.Reset3GModuleWithMandatoryBasicSetup(_sp3GModule, _reset3GModule);
                                InitializeGPSUnit();
                                state = 1;
                                break;
                            }
                            else
                            {
                                //just keep checking, set state = 3, looping here until we are able to registered                               
                                state = 3;
                                Thread.Sleep(2000);  //wait 2 seconds
                                break;
                            }
                        }

                    case 4:                        
                        //Deactivate Context first, don't care if it is activated or not
                        util.DeactivatePDPContext(_sp3GModule);
                       
                        //Activate PDP base on credential or not
                        if (String.Compare(_apn_username, String.Empty) == 0 && String.Compare(_apn_password, String.Empty) == 0)
                            PDPContextActivated = util.ActivatePDPContext(_sp3GModule);
                        else
                            PDPContextActivated = util.ActivatePDPContext(_sp3GModule, _apn_username, _apn_password);

                        if (PDPContextActivated == true)
                        {                           
                            //Get SIM phone number
                            _SIMPhoneNo = util.GetSubscriberPhoneNumber(_sp3GModule);
                                                        
                            break;
                        }
                        else
                        {
                            //Do hard reset with basic mandatory setup, then initialize GPS, then go back to state = 1
                            //This basically repeat the same initialization step when the system is reset or reboot
                            util.Reset3GModuleWithMandatoryBasicSetup(_sp3GModule, _reset3GModule);
                            InitializeGPSUnit();                            
                            state = 1;
                            break;
                        }                            
                }
 
                if (PDPContextActivated == true)
                    break;

            } //end while loop


            //PDP Context is activated, now we try to
            //loop until we get network date time
            bool setRTCFromNetworkOK = false;
            state = 1;            
            while (true)
            {
                switch (state)
                {
                    case 1:
                        //Try to stop GPSThread from disrupting setting up and acquiring network date time
                        //wait until the serial port is free
                        while (SpecialGlobalVar._3GModuleSerialPortBusy)
                        {
                            //wait until it is free
                        }
                        SpecialGlobalVar._3GModuleSerialPortBusy = true;
                        _spCOM3.Flush();
                        _spCOM3.DiscardInBuffer();
                        _spCOM3.DiscardOutBuffer();
                        _spCOM3.Flush();

                        while (SpecialGlobalVar._GPSThreadIsOnHold == false)
                        {
                            //Wait here until GPS Thread is on hold
                        }

                        if (util.EnableNetworkAutomaticUpdatingDateTime(_sp3GModule) == true)
                        {
                            state = 2;
                        }
                        else
                        {
                            state = 1; //keep trying to make sure the command is set in the module                            
                        }
                        break;

                    case 2:
                        if (util.SetClockModeToUTCFormat(_sp3GModule) == true)
                        {
                            state = 3;
                            _3gOnlineLED.Write(true);
                            _3GOnline = true;
                        }
                        else
                        {
                            state = 2; //keep trying to make sure the command is set in the module
                        }
                        break;

                    case 3:
                        //Set the RTC using network time here
                        int[] networkDT = util.GetCellularNetworkDateTime(_sp3GModule);

                        if (networkDT != null && networkDT.Length == 6)
                        {
                            RealTimeClock.SetDateTime(new DateTime(networkDT[0], networkDT[1], networkDT[2], networkDT[3], networkDT[4], networkDT[5]));
                            Utility.SetLocalTime(RealTimeClock.GetDateTime());

                            //Write RTC flag to flash memory using extended weak reference
                            ExtendedWeakReference ewrObject = ExtendedWeakReference.RecoverOrCreate(typeof(bool), 0, ExtendedWeakReference.c_SurvivePowerdown);
                            ewrObject.Target = true;
                            ExtendedWeakReference.FlushAll();

                            _IsRTCSet = true;
                            setRTCFromNetworkOK = true;
                        }

                        //Reset flag to let GPS Thread run again
                        SpecialGlobalVar._3GModuleSerialPortBusy = false;

                        break;
                }

                if (setRTCFromNetworkOK == true)
                    break;

            }//end while loop
           

            util = null;
        }

        //Initialize GPS unit
        public static bool InitializeGPSUnit()
        {
            //set flag
            SpecialGlobalVar._3GModuleSerialPortBusy = true;

            HUtility util = new HUtility();
            _gpsOnlineLED.Write(false);
            _GPSOnline = false;      
            int state = 1;  //start at state 1           
            //Loop until GPS unit is initialized
            while (true)
            {               
                switch(state)
                {
                    case 0:
                        _gpsOnlineLED.Write(false);
                        _GPSOnline = false;      
                        //stop location request and start over...
                        if (util.SendCmdToUART(_sp3GModule, "AT$GPSSTOP=0", "OK", 1000, true) == true)
                        {
                            state = 1;
                        }
                        else 
                        {
                            if (util.SendCmdToUART(_sp3GModule, "AT$GPSSTOP=0", "ERROR", 1000, true) == true)
                            {
                                state = 1;
                            }
                            else
                            {
                                //sytem need reset, odd problem here
                                SpecialGlobalVar._SystemNeedReboot = true;
                                Thread.Sleep(Timeout.Infinite);  //wait until watchdog reset the system
                                break;
                            }

                        }
                        break;

                    case 1:
                        //start location request, get gps coordinate every 2 seconds
                        if (util.SendCmdToUART(_sp3GModule, "AT$GPSSLSR=2,3,,,,,2,,", "OK", 1000, true) == true)
                            state = 2;
                        else
                            state = 0;                        
                        break;   
                     
                    case 2:
                        //set GPS NMEA data configuration
                        if (util.SendCmdToUART(_sp3GModule, "AT$GPSNMUN=0,,,,,1,", "OK", 1000, true) == true)
                        {
                            _gpsOnlineLED.Write(true);
                            _GPSOnline = true;
                            //save GPS setting
                            util.SaveGPSParameters(_sp3GModule);
                            break;
                        }
                        else
                        {
                            state = 0;
                        }
                        break;                                            
                }

                if (_GPSOnline == true)
                    break;

            }//End GPS initialize loop

            //reset flag
            SpecialGlobalVar._3GModuleSerialPortBusy = false;

            util = null;
            return true;
        }

        public static IOData GetIOReadings()
        {
            double temp1 = 0, temp2 = 0, analog1 = 0;
            int on_off_1 = 0;
            _ds18B20_1 = new DS18B20(_tempPin_1);
            _ds18B20_2 = new DS18B20(_tempPin_2);
            //Get temperature IO first
            temp1 = _ds18B20_1.GetTempInCelsius();
            temp2 = _ds18B20_2.GetTempInCelsius();

            //Get on off reading IO
            if (_onoff_1.Read() == true)
                on_off_1 = 0;
            else
                on_off_1 = 1;
           
            //Get 0-5V analog reading            
            analog1 = _analog_0_5V_1.Read();
            analog1 = analog1 * 3.3;
            analog1 = analog1 / 0.66;           

            return new IOData(temp1.ToString(), temp2.ToString(), on_off_1.ToString(), analog1.ToString());
        }

        public static byte[] PadLeft(byte[] input, int totalLength)
        {
            if (input.Length >= totalLength)
            {
                return input;
            }

            byte[] padded = new byte[totalLength];
            int padStart = totalLength - input.Length;

            for (int i = 0; i < totalLength; i++)
            {
                if (i < padStart)
                {
                    padded[i] = 0;
                }
                else
                {
                    padded[i] = input[i - padStart];
                }
            }
            return padded;
        }

        public static byte[] PadRight(byte[] input, int totalLength)
        {
            if (input.Length >= totalLength)
            {
                return input;
            }
            byte[] result = new byte[totalLength];

            for (int i=0; i<totalLength; i++)
            {
                if (i < input.Length - 1)
                {
                    result[i] = input[i];
                }
                else
                {
                    return result;
                }
            }
            return result;
        }

        public static uint Do2complement(int value)
        {
            //make it positive first
            value = value * -1;

            //assign to unsigned integer
            uint new_value = (uint)value;

            new_value = ~new_value + 1;
            return new_value;
        }
       
        public static byte[] CreateBytesPackageToSend(IOData iodata)
        {
            HUtility util = new HUtility();                      
            ArrayList data_package_byte_array = new ArrayList();            
                    
            //data package start
            //Package type byte, 0xDD - regular data package
            byte[] package_type = BitConverter.GetBytes(221);
            data_package_byte_array.Add(new MyByte(package_type[0]));   

            //Production year and hardware version, hardware version contain production year        
            byte[] serial_header_b_array = BitConverter.GetBytes(_HW_VER);            
            data_package_byte_array.Add(new MyByte(serial_header_b_array[0]));
            data_package_byte_array.Add(new MyByte(serial_header_b_array[1]));
           
            //Hardware Serial counter
            byte[] serial_counter_b_array = PadRight(BitConverter.GetBytes(_HW_SERIAL), 2);        
            data_package_byte_array.Add(new MyByte(serial_counter_b_array[0]));
            data_package_byte_array.Add(new MyByte(serial_counter_b_array[1]));

            //Firmware version
            byte[] firmware_ver = BitConverter.GetBytes(_FW_VER);
            data_package_byte_array.Add(new MyByte(firmware_ver[0]));            
            
            //Sensors data bytes length
            data_package_byte_array.Add(new MyByte(0x00));
            data_package_byte_array.Add(new MyByte(0x00));
           
            //Setting ID
            byte[] settingID = PadRight(BitConverter.GetBytes(int.Parse(_setting_id)), 4);
            data_package_byte_array.Add(new MyByte(settingID[0]));
            data_package_byte_array.Add(new MyByte(settingID[1]));
            data_package_byte_array.Add(new MyByte(settingID[2]));
            data_package_byte_array.Add(new MyByte(settingID[3]));

            //epoch date time, what datetime we need to put here?  Normally if gps is locked and updating, using _GPSDataObject is fine.
            //What about when gps is not locked or updating? We must check to see if RTC is set, if set, then use RTC or system datetime.
            //If RTC is not set, we must include a default time for HUB to know that datetime from the unit is invalid, HUB then can use 
            //HUB's system datetime.
            long epochTime = 0;
            if (_GPSLocked == true)
                epochTime = ToEpochTicks(_GPSDataObject.UTCDateTime);                
            else if (_GPSLocked == false && _IsRTCSet == true)
                epochTime = ToEpochTicks(DateTime.Now);
            else
                epochTime = ToEpochTicks(new DateTime(1980, 5, 13, 0, 0, 0));
                                    
            byte[] epochTime_b_array = PadRight(BitConverter.GetBytes((uint)epochTime), 4);
            data_package_byte_array.Add(new MyByte(epochTime_b_array[0]));
            data_package_byte_array.Add(new MyByte(epochTime_b_array[1]));
            data_package_byte_array.Add(new MyByte(epochTime_b_array[2]));
            data_package_byte_array.Add(new MyByte(epochTime_b_array[3]));            

            //longitude and latitude
            double dlatDecimal = Convert.ToDouble(util.GetLongLatDecimal(_GPSDataObject.Latitude, true)) * 1000000;
            int latitude = (int)dlatDecimal;
            double dlongDecimal = Convert.ToDouble(util.GetLongLatDecimal(_GPSDataObject.Longitude, false)) * 1000000;
            int longitude = (int)dlongDecimal;

            //convert to 2's complement if the number is negative
            uint u_latitude = 0, u_longitude = 0;
            if (latitude < 0)
            {
                u_latitude = Do2complement(latitude);
            }
            else
            {
                u_latitude = (uint)latitude;
            }

            if (longitude < 0)
            {
                u_longitude = Do2complement(longitude);
            }
            else
            {
                u_longitude = (uint)longitude;
            }

            byte[] latitude_b_array = PadRight(BitConverter.GetBytes(u_latitude), 4);
            data_package_byte_array.Add(new MyByte(latitude_b_array[0]));
            data_package_byte_array.Add(new MyByte(latitude_b_array[1]));
            data_package_byte_array.Add(new MyByte(latitude_b_array[2]));
            data_package_byte_array.Add(new MyByte(latitude_b_array[3]));

            byte[] longitude_b_array = PadRight(BitConverter.GetBytes(u_longitude), 4);
            data_package_byte_array.Add(new MyByte(longitude_b_array[0]));
            data_package_byte_array.Add(new MyByte(longitude_b_array[1]));
            data_package_byte_array.Add(new MyByte(longitude_b_array[2]));
            data_package_byte_array.Add(new MyByte(longitude_b_array[3]));           
            
            //Start looping through _registeredSensor to see what wired and wireless sensor are required
            //Get the current datetime from RTC            
            DateTime currentDT = DateTime.Now;           
            foreach (var sensorObject in _SensorList)
            {
                //If sensor is point six wireless sensor
                if (sensorObject is SensorRecord)
                {
                    SensorRecord sensor = sensorObject as SensorRecord;

                    //Check for which io are required
                    if (sensor.IOFlag == 1)  //io1 only
                    {                        
                        byte[] io1_b_array = PadRight(BitConverter.GetBytes(sensor.RawIOValue1), 2);
                        data_package_byte_array.Add(new MyByte(io1_b_array[0]));
                        data_package_byte_array.Add(new MyByte(io1_b_array[1]));
                    }
                    else if (sensor.IOFlag == 2) //io2 only
                    {
                        byte[] io2_b_array = PadRight(BitConverter.GetBytes(sensor.RawIOValue2), 2);
                        data_package_byte_array.Add(new MyByte(io2_b_array[0]));
                        data_package_byte_array.Add(new MyByte(io2_b_array[1]));
                    }
                    else //both io1 and io2 are required
                    {
                        //raw reading
                        byte[] ios_b_array = PadRight(BitConverter.GetBytes(sensor.RawIOValue1), 2);
                        data_package_byte_array.Add(new MyByte(ios_b_array[0]));
                        data_package_byte_array.Add(new MyByte(ios_b_array[1]));

                        ios_b_array = PadRight(BitConverter.GetBytes(sensor.RawIOValue2), 2);
                        data_package_byte_array.Add(new MyByte(ios_b_array[0]));
                        data_package_byte_array.Add(new MyByte(ios_b_array[1]));
                    }

                    //battery reading, this can be 0-100, 101 => battery was not reset from last battery change or need to change battery immediately
                    //102 => the sensor does not support battery reading, 103 => sensor is offline, no IOs readings or battery reading
                    
                    //Check for sensor offline first
                    int timeDiffInMinutes = 0;
                    timeDiffInMinutes = currentDT.Subtract(sensor.TimeStamp).Minutes;
                    byte[] battery_b_array;
                    if (timeDiffInMinutes > _wireless_sensor_offline_threshold)
                    {
                        battery_b_array = BitConverter.GetBytes(103);
                        data_package_byte_array.Add(new MyByte(battery_b_array[0]));
                    }
                    else
                    {
                        battery_b_array = BitConverter.GetBytes(sensor.Battery);
                        data_package_byte_array.Add(new MyByte(battery_b_array[0]));
                    }                    
                }

                //If sensor is wired grps port
                if (sensorObject is GPRSPort)
                {
                    //Check to see which port is required
                    GPRSPort sensor = sensorObject as GPRSPort;
                    if (String.Compare(sensor.PortName, "io1") == 0)  
                    {
                        double d_temp1 = Convert.ToDouble(iodata.Temp1) * 100;
                        int temp1 = (int)d_temp1;

                        //convert to 2's complement if the number is negative
                        uint u_temp1 = 0; ushort utemp1 = 0;

                        if (temp1 < 0)
                        {
                            u_temp1 = Do2complement(temp1);
                            utemp1 = (ushort)u_temp1;
                        }
                        else
                        {
                            utemp1 = (ushort)temp1;
                        }

                        byte[] temp1_b_array = PadRight(BitConverter.GetBytes(utemp1), 2);
                        data_package_byte_array.Add(new MyByte(temp1_b_array[0]));
                        data_package_byte_array.Add(new MyByte(temp1_b_array[1]));                        
                    }
                    else if (String.Compare(sensor.PortName, "io2") == 0)
                    {
                        double d_temp2 = Convert.ToDouble(iodata.Temp2) * 100;
                        int temp2 = (int)d_temp2;

                        //convert to 2's complement if the number is negative
                        uint u_temp2 = 0; ushort utemp2 = 0;

                        if (temp2 < 0)
                        {
                            u_temp2 = Do2complement(temp2);
                            utemp2 = (ushort)u_temp2;
                        }
                        else
                        {
                            utemp2 = (ushort)temp2;
                        }

                        byte[] temp2_b_array = PadRight(BitConverter.GetBytes(utemp2), 2);
                        data_package_byte_array.Add(new MyByte(temp2_b_array[0]));
                        data_package_byte_array.Add(new MyByte(temp2_b_array[1]));                        
                    }
                    else if (String.Compare(sensor.PortName, "io3") == 0)
                    {
                        //On, Off byte
                        int onoff1 = Convert.ToInt32(iodata.OnOff1);

                        if (onoff1 == 1)
                            data_package_byte_array.Add(new MyByte(0x01));
                        else
                            data_package_byte_array.Add(new MyByte(0x00));                       
                    }
                    else if (string.Compare(sensor.PortName, "io4") == 0)
                    {
                        //Voltage 0-5V
                        double d_voltage1 = Convert.ToDouble(iodata.Analog1) * 100;
                        int voltage1 = (int)d_voltage1;

                        byte[] volt1_b_array = PadRight(BitConverter.GetBytes(voltage1), 2);
                        data_package_byte_array.Add(new MyByte(volt1_b_array[0]));
                        data_package_byte_array.Add(new MyByte(volt1_b_array[1]));                                              
                    }
                }
            }//end for
            

            byte[] full_package = new byte[data_package_byte_array.Count + 6];   //2bytes Header, 2bytes CRC16, 2bytes Footer
            byte[] data_package_without_headers_and_crc = new byte[data_package_byte_array.Count];
            
            MyByte myByte;
            for (int i = 0; i < data_package_byte_array.Count; i++)
            {
                myByte = data_package_byte_array[i] as MyByte;
                data_package_without_headers_and_crc[i] = myByte.TheByte;
            }

           // Debug.Print("Raw, no header or crc: " + HTools.ByteArr.ToHex(data_package_without_headers_and_crc));

            //package length
            byte[] sensorDataPackageLengthByteArray = PadRight(BitConverter.GetBytes(full_package.Length), 2);
            data_package_without_headers_and_crc[6] = sensorDataPackageLengthByteArray[0];
            data_package_without_headers_and_crc[7] = sensorDataPackageLengthByteArray[1];

            //calculate xor and crc
            //get xor first                                   
            byte[] xor_data_package = util.PassPharseXOR(data_package_without_headers_and_crc, _xorPass);            
            //get crc
            byte[] crc = Crc16.ComputeBytes(xor_data_package);


            //add header "HD"
            full_package[0] = 0x48;
            full_package[1] = 0x44;

            //Add xor_data_package to full_package
            Array.Copy(xor_data_package, 0, full_package, 2, xor_data_package.Length);

            //add crc
            full_package[xor_data_package.Length + 2] = crc[0];
            full_package[xor_data_package.Length + 3] = crc[1];

            //end package
            full_package[xor_data_package.Length + 4] = 0x44;
            full_package[xor_data_package.Length + 5] = 0x48;

            util = null;

            //Debug.Print("Full XOR and CRC: " + HTools.ByteArr.ToHex(full_package));
            
            return full_package;            
        }

        public static Queue CreateDownloadLogBytePackagesForPointSixSensor(Queue sensorRecordQueue, int maxNumberOfSensorRecord)
        {
            if (sensorRecordQueue == null) //null variable, return null
                return null;

            if (sensorRecordQueue.Count <= 0)  //empty list, just return null.
                return null;

            HUtility util = new HUtility();
            Queue downloadLogStringByteQueue = new Queue();

            //Keep going until no more sensor record in the array list
            while (sensorRecordQueue.Count > 0)
            {                
                ArrayList data_package_byte_array = new ArrayList();

                //data package start
                //Package type byte, 0xD0 - downloaded log data package
                byte[] package_type = BitConverter.GetBytes(208);
                data_package_byte_array.Add(new MyByte(package_type[0]));

                //Production year and hardware version, hardware version contain production year        
                byte[] serial_header_b_array = BitConverter.GetBytes(_HW_VER);
                data_package_byte_array.Add(new MyByte(serial_header_b_array[0]));
                data_package_byte_array.Add(new MyByte(serial_header_b_array[1]));

                //Hardware Serial counter
                byte[] serial_counter_b_array = PadRight(BitConverter.GetBytes(_HW_SERIAL), 2);
                data_package_byte_array.Add(new MyByte(serial_counter_b_array[0]));
                data_package_byte_array.Add(new MyByte(serial_counter_b_array[1]));

                //Firmware version
                byte[] firmware_ver = BitConverter.GetBytes(_FW_VER);
                data_package_byte_array.Add(new MyByte(firmware_ver[0]));

                //Sensors data bytes length
                data_package_byte_array.Add(new MyByte(0x00));
                data_package_byte_array.Add(new MyByte(0x00));

                //Setting ID
                byte[] settingID = PadRight(BitConverter.GetBytes(int.Parse(_setting_id)), 4);
                data_package_byte_array.Add(new MyByte(settingID[0]));
                data_package_byte_array.Add(new MyByte(settingID[1]));
                data_package_byte_array.Add(new MyByte(settingID[2]));
                data_package_byte_array.Add(new MyByte(settingID[3]));

                //Sensor Serial Number 
                object obj = sensorRecordQueue.Peek();
                SensorRecord sr = obj as SensorRecord;             
                UInt64 sensor_serial_in_dec = util.ConvertToUInt64(sr.SensorID);
                byte[] sensor_serial = PadRight(BitConverter.GetBytes(sensor_serial_in_dec), 8);
                data_package_byte_array.Add(new MyByte(sensor_serial[0]));
                data_package_byte_array.Add(new MyByte(sensor_serial[1]));
                data_package_byte_array.Add(new MyByte(sensor_serial[2]));
                data_package_byte_array.Add(new MyByte(sensor_serial[3]));
                data_package_byte_array.Add(new MyByte(sensor_serial[4]));
                data_package_byte_array.Add(new MyByte(sensor_serial[5]));
                data_package_byte_array.Add(new MyByte(sensor_serial[6]));
                data_package_byte_array.Add(new MyByte(sensor_serial[7]));


                //Create sensor reading and add to the package one by one                
                long epochTime = 0;
                int for_loop_count = 0;
                //Determine how many sensor records left
                int remainingSensorRecord = sensorRecordQueue.Count;
                if (remainingSensorRecord >= maxNumberOfSensorRecord)
                    for_loop_count = maxNumberOfSensorRecord;
                else
                    for_loop_count = remainingSensorRecord;

                for (int i = 0; i < for_loop_count; i++)                                                    
                {                    
                    sr = sensorRecordQueue.Dequeue() as SensorRecord;

                    //Get date time  
                    epochTime = ToEpochTicks(sr.TimeStamp);                    
                    byte[] epochTime_b_array = PadRight(BitConverter.GetBytes((uint)epochTime), 4);
                    data_package_byte_array.Add(new MyByte(epochTime_b_array[0]));
                    data_package_byte_array.Add(new MyByte(epochTime_b_array[1]));
                    data_package_byte_array.Add(new MyByte(epochTime_b_array[2]));
                    data_package_byte_array.Add(new MyByte(epochTime_b_array[3]));

                    //Get IOs depend on the setting
                    //Check for which io are required
                    if (sr.IOFlag == 1)  //io1 only
                    {
                        byte[] io1_b_array = PadRight(BitConverter.GetBytes(sr.RawIOValue1), 2);
                        data_package_byte_array.Add(new MyByte(io1_b_array[0]));
                        data_package_byte_array.Add(new MyByte(io1_b_array[1]));
                    }
                    else if (sr.IOFlag == 2) //io2 only
                    {
                        byte[] io2_b_array = PadRight(BitConverter.GetBytes(sr.RawIOValue2), 2);
                        data_package_byte_array.Add(new MyByte(io2_b_array[0]));
                        data_package_byte_array.Add(new MyByte(io2_b_array[1]));
                    }
                    else //both io1 and io2 are required
                    {
                        byte[] ios_b_array = PadRight(BitConverter.GetBytes(sr.RawIOValue1), 2);
                        data_package_byte_array.Add(new MyByte(ios_b_array[0]));
                        data_package_byte_array.Add(new MyByte(ios_b_array[1]));

                        ios_b_array = PadRight(BitConverter.GetBytes(sr.RawIOValue2), 2);
                        data_package_byte_array.Add(new MyByte(ios_b_array[0]));
                        data_package_byte_array.Add(new MyByte(ios_b_array[1]));
                    }

                    //Add battery reading
                    byte[] battery_b_array = BitConverter.GetBytes(sr.Battery);
                    data_package_byte_array.Add(new MyByte(battery_b_array[0]));

                }

                byte[] full_package = new byte[data_package_byte_array.Count + 6];   //2bytes Header, 2bytes CRC16, 2bytes Footer
                byte[] data_package_without_headers_and_crc = new byte[data_package_byte_array.Count];

                MyByte myByte;
                for (int i = 0; i < data_package_byte_array.Count; i++)
                {
                    myByte = data_package_byte_array[i] as MyByte;
                    data_package_without_headers_and_crc[i] = myByte.TheByte;
                }

                // Debug.Print("Raw, no header or crc: " + HTools.ByteArr.ToHex(data_package_without_headers_and_crc));

                //package length
                byte[] sensorDataPackageLengthByteArray = PadRight(BitConverter.GetBytes(full_package.Length), 2);
                data_package_without_headers_and_crc[6] = sensorDataPackageLengthByteArray[0];
                data_package_without_headers_and_crc[7] = sensorDataPackageLengthByteArray[1];

                //calculate xor and crc
                //get xor first                                   
                byte[] xor_data_package = util.PassPharseXOR(data_package_without_headers_and_crc, _xorPass);
                //get crc
                byte[] crc = Crc16.ComputeBytes(xor_data_package);

                //add header "HD"
                full_package[0] = 0x48;
                full_package[1] = 0x44;

                //Add xor_data_package to full_package
                Array.Copy(xor_data_package, 0, full_package, 2, xor_data_package.Length);

                //add crc
                full_package[xor_data_package.Length + 2] = crc[0];
                full_package[xor_data_package.Length + 3] = crc[1];

                //end package
                full_package[xor_data_package.Length + 4] = 0x44;
                full_package[xor_data_package.Length + 5] = 0x48;

                //Add current package to ArrayList
                downloadLogStringByteQueue.Enqueue(util.ByteArrayToHexString(full_package));

            }//end while loop

            util = null;

            // Debug.Print("Full XOR and CRC: " + HTools.ByteArr.ToHex(full_package));

            return downloadLogStringByteQueue;                                       
        }

        public static void UploadHistoryLog(LOG_FILE_TYPE logFileType)
        {
            //Try to see if any data log file exist if so read and send all the recorded data package     
            HUB_RESPONSE hub_response = new HUB_RESPONSE(HUB_RESPONSE_CODE.NO_RESPONSE_3G_MODULE_TIMEOUT, null);            
                                                
            while (true)
            {             
                ArrayList dataLogList = _sdStor.ReadDataLogFile(logFileType);                
                if (dataLogList == null)
                {
                    break;
                }

                if (dataLogList.Count <= 0)
                {
                    break;
                }

                //Start sending log data     
                int MAX_SEND_DATA_PACKAGE = 5;

                if (logFileType == LOG_FILE_TYPE.REGULAR_LOG_FILE)
                    MAX_SEND_DATA_PACKAGE = 5;
                else
                    MAX_SEND_DATA_PACKAGE = 1;

                string history_data = null;
                ArrayList logs_to_send_list = new ArrayList();         
                while (dataLogList.Count > 0)
                {
                    //take out data package to send, up to MAX_SEND_DATA_PACKAGE size
                    int counter = 0;
                    foreach (var log in dataLogList)
                    {
                        if (counter >= MAX_SEND_DATA_PACKAGE)
                        {
                            counter = 0;  //reset counter
                            break;
                        }

                        logs_to_send_list.Add(log);  //add to send list
                        counter++;
                    }


                    //turn send list to one string
                    foreach (var log in logs_to_send_list)
                    {
                        if (log == null)
                            continue;
                        if (String.Compare(log.ToString(), "") == 0)
                            continue;

                        history_data += log;
                    }

                    //if history_data is empty continue
                    if (String.Compare(history_data, "") == 0 || history_data == null)
                    {
                        foreach (var log in logs_to_send_list)
                        {
                            dataLogList.Remove(log);
                        }
                        continue;
                    }


                    //Send history log data package to server, we also need to reset the watchdog every send because it could be a long log file
                    HUtility util = new HUtility();
                    byte[] byte_dataPackage = util.HexStringToByteArray(history_data);
                    ResetWatchDog();
                    hub_response = util.SendMsgOverTCPConnectionToHub(_sp3GModule, byte_dataPackage, 500, 40, false);
                    ResetWatchDog();

                    //send history data log but no response from HUB or 3G module timeout or not
                    if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.NO_RESPONSE_3G_MODULE_TIMEOUT ||
                        hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.NO_RESPONSE_3G_MODULE_DID_NOT_TIMEOUT)     
                    {
                        //put back to the log into Data text file
                        if (logFileType == LOG_FILE_TYPE.REGULAR_LOG_FILE)
                        {
                            _sdStor.WriteUndeliverableDataToDataLogFile(dataLogList, _LogFileIndex, out _LogFileIndex, MAX_LOG_FILE_SIZE, logFileType);
                            ResetWatchDog();
                            break;
                        }
                        else
                        {
                            _sdStor.WriteUndeliverableDataToDataLogFile(dataLogList, _DownloadLogFileIndex, out _DownloadLogFileIndex, MAX_LOG_FILE_SIZE, logFileType);
                            ResetWatchDog();
                            break;
                        }                        
                    }                   
                    else  //send history data log OK or Hub response NOT OK.  If NOT OK (NOK) received from Hub, still remove from  list because we don't want to store invalid packages.                                   
                    {
                        //remove send history logs from dataLogList
                        foreach (var log in logs_to_send_list)
                        {
                            dataLogList.Remove(log);
                        }

                        //reset variables                                            
                        history_data = "";
                        logs_to_send_list.Clear();
                        continue;
                    }

                }//end while loop                                

                //if no response from HUB, HUB might be slow or down or in some kind fo trouble, just stop uploading
                if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.NO_RESPONSE_3G_MODULE_TIMEOUT ||
                    hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.NO_RESPONSE_3G_MODULE_DID_NOT_TIMEOUT)  
                {
                    ResetWatchDog();
                    break;
                }

            }//end while loop   

            Debug.GC(true);
        }

        //padleft for string
        public static string StringPadleft(string str, char padChar, int total_padded_length)
        {
            if (str.Length >= total_padded_length)
                return str;


            int length = total_padded_length - str.Length;
            string newStr = "";
            for (int i = 0; i < length; i++)
            {
                newStr = newStr + padChar;
            }
            newStr = newStr + str;
            return newStr;
        }
        
        //Handle Hub request for setting, handle command REQ_SETTING
        public static HUB_RESPONSE HandleHubRequestForSetting()
        {
            HUtility util  = new HUtility();
            HUB_RESPONSE response = new HUB_RESPONSE(HUB_RESPONSE_CODE.NO_RESPONSE_3G_MODULE_TIMEOUT, null);                        
                          
            //Create setting package
            string serial = _HW_VER.ToString() + StringPadleft(_HW_SERIAL.ToString(), '0', 5);
            string fw_ver = _FW_VER.ToString();
            string name = _DEVICE_NAME;
            string transmit_int = _TRANSMIT_INTERVAL.ToString();  //convert to minutes
            string ip = _ip;
            string port = _port;
            string sim_number = _SIMPhoneNo;
            string apn_name = _apn_name;
            string apn_username = _apn_username;
            string apn_password = _apn_password;
            string offline_threshold = _wireless_sensor_offline_threshold.ToString();
            string setting_id = _setting_id;
            string sensors_definition = _sensors_definition;
            StringBuilder sdsb = new StringBuilder(_sensors_definition);

            string setting = "";
            if (String.Compare(_sensors_definition, String.Empty) != 0)
            {
                sdsb.Replace("DEVICE_SERIAL", serial);

                setting = serial + "|" + fw_ver + "|" + name + "|" + transmit_int + "|" + offline_threshold + "|" + ip + "|" + port + "|" + sim_number + "|" + _apn_name + "|" + apn_username + "|" +
                                apn_password + "|" + _epoch_time_stamp + "|" + _setting_id + "|" + sdsb.ToString().Trim() + "|";
            }
            else
            {
                setting = serial + "|" + fw_ver + "|" + name + "|" + transmit_int + "|" + offline_threshold + "|" + ip + "|" + port + "|" + sim_number + "|" + _apn_name + "|" + apn_username + "|" +
                                apn_password + "|" + _epoch_time_stamp + "|" + _setting_id + "|" + "" + "|";
            }
            

            
            //CRC computation
            byte[] crc = Crc16.ComputeBytes(Encoding.UTF8.GetBytes(setting));
            byte[] reverse_crc = new byte[2];
            reverse_crc[0] = crc[1];
            reverse_crc[1] = crc[0];
            string crcStr = HTools.ByteArr.ToHex(reverse_crc);
            string settingStr = HTools.ByteArr.ToHex(Encoding.UTF8.GetBytes(setting));
            
            //Debug.Print(settingStr);

            //Xor with passphrase
            byte[] xor_setting_package_byte_array = util.PassPharseXOR(Encoding.UTF8.GetBytes(setting + crcStr), _xorPass);

            byte[] headerByte1 = Encoding.UTF8.GetBytes("DOK");
            byte[] headerByte2 = Encoding.UTF8.GetBytes("REQ_SETTING");                
                
            ArrayList settingByteArray = new ArrayList();
            foreach (byte b in headerByte1)
            {
                settingByteArray.Add(new MyByte(b));
            }
            settingByteArray.Add(new MyByte(10));
               
            foreach (byte b in headerByte2)
            {
                settingByteArray.Add(new MyByte(b));
            }
            settingByteArray.Add(new MyByte(10));
               
            foreach (byte b in xor_setting_package_byte_array)
            {
                settingByteArray.Add(new MyByte(b));
            }
                               
            byte[] sendByte = new byte[settingByteArray.Count + 1];  
            MyByte myByte;
            for (int i = 0; i < settingByteArray.Count; i++)
            {
                myByte = settingByteArray[i] as MyByte;
                sendByte[i] = myByte.TheByte;
            }
               
            //Line feed added to the end
            sendByte[sendByte.Length - 1] = 0x0A;

            ResetWatchDog();
            response = util.SendMsgOverTCPConnectionToHub(_sp3GModule, sendByte, 2000, 20, false);
            ResetWatchDog();
                            
            return response;           
        }

        //Handle hub command for set setting, handle command SET_SETTING
        //This also force the device set the RTC flag to false, this will force the GPS Thread to up date the RTC using network date time or GPS date time
        public static void HandleHubCommandForSetSetting(byte[] hub_reponse_byte_array)
        {
            if (hub_reponse_byte_array == null)
                return;

            HUtility util  = new HUtility();            
            HUB_RESPONSE response = new HUB_RESPONSE(HUB_RESPONSE_CODE.NO_RESPONSE_3G_MODULE_TIMEOUT, null);
            string reply = null;

            //Start parsing response for settings
            string[] res_lines = util.ByteArrayToStringLines(hub_reponse_byte_array);
            //Check for proper response, should contain only 3 lines, HOK, SET_SETTING, PAYLOAD
            if (res_lines.Length < 3)
                return;

            string[] payload_lines = res_lines[2].Split('|');
            //Check for proper payload package, only 14 lines
            if (payload_lines.Length != 15)
                return;          

            //Verify CRC
            string org_crc_str = payload_lines[14];
            string setting_str = res_lines[2].Substring(0, res_lines[2].Length - 4);
            byte[] cal_crc = Crc16.ComputeBytes(Encoding.UTF8.GetBytes(setting_str));
            string cal_crc_str = HTools.ByteArr.ToHex(cal_crc);

            if (String.Compare(org_crc_str.ToLower(), cal_crc_str.ToLower()) != 0)
            {
                reply = "INVALID_CHECKSUM\n" + "SET_SETTING\n" + res_lines[2] + '\n';
                util.SendMsgOverTCPConnectionToHub(_sp3GModule, Encoding.UTF8.GetBytes(reply), true);                
                
                //set null for GC to collect
                reply = null; util = null; response = null; res_lines = null; payload_lines = null;
                return;
            }
            
            //Check for correct serial number
            string dev_serial = _HW_VER.ToString() + StringPadleft(_HW_SERIAL.ToString(), '0', 5);
            if (String.Compare(payload_lines[0].Trim(), dev_serial) != 0)
            {
                reply = "INVALID_SERIAL\n" + "SET_SETTING\n" + res_lines[2] + '\n';
                util.SendMsgOverTCPConnectionToHub(_sp3GModule, Encoding.UTF8.GetBytes(reply), true);

                //set null for GC to collect
                reply = null; util = null; response = null; res_lines = null; payload_lines = null; dev_serial = null;
                return;
            }

            //Check for correct firmware version
            if (String.Compare(payload_lines[1].Trim(), _FW_VER.ToString()) != 0)
            {
                reply = "INVALID_FW_VER\n" + "SET_SETTING\n" + res_lines[2] + '\n';
                util.SendMsgOverTCPConnectionToHub(_sp3GModule, Encoding.UTF8.GetBytes(reply), true);

                //set null for GC to collect
                reply = null; util = null; response = null; res_lines = null; payload_lines = null; dev_serial = null;
                return;
            }

            //Apply new setting by writing to settings.txt file and force reset of RTC            
            try
            {   
                //Set RTC flag to false
                //Write RTC flag to flash memory using extended weak reference
                ExtendedWeakReference ewrObject = ExtendedWeakReference.RecoverOrCreate(typeof(bool), 0, ExtendedWeakReference.c_SurvivePowerdown);
                ewrObject.Target = false;
                ExtendedWeakReference.FlushAll();
                _IsRTCSet = false;

                //set to code variable
                _DEVICE_NAME = payload_lines[2].Trim();
                _TRANSMIT_INTERVAL = int.Parse(payload_lines[3].Trim());
                _wireless_sensor_offline_threshold = int.Parse(payload_lines[4].Trim());
                _ip = payload_lines[5].Trim();
                _port = payload_lines[6].Trim();                
                _apn_name = payload_lines[8].Trim();
                _apn_username = payload_lines[9].Trim();
                _apn_password = payload_lines[10].Trim();
                _epoch_time_stamp = payload_lines[11].Trim();
                _setting_id = payload_lines[12].Trim();
                _sensors_definition = payload_lines[13].Trim();

                //Registered newly sensors definition, this takes long so we need to reset the watchdog
                ResetWatchDog();
                RegisterWiredAndWirelessSensorsConfiguration(_sensors_definition);
                ResetWatchDog();

                //Overwrite settings.txt file    
                StringBuilder sb = new StringBuilder();                

                sb.AppendLine("PROCESS_GPS_DATA_INTERVAL = " + _PROCESS_GPS_DATA_INTERVAL.ToString());
                sb.AppendLine("DEVICE_NAME = " + _DEVICE_NAME);
                sb.AppendLine("TRANSMIT_INTERVAL = " + _TRANSMIT_INTERVAL.ToString());
                sb.AppendLine("WIRELESS_SENSOR_OFFLINE_THRESHOLD = " + _wireless_sensor_offline_threshold.ToString());
                sb.AppendLine("IP = " + _ip);
                sb.AppendLine("PORT = " + _port);
                sb.AppendLine("DEVICE_PHONE_NO = " + _SIMPhoneNo);
                sb.AppendLine("APN_NAME = " + _apn_name);
                sb.AppendLine("APN_USERNAME = " + _apn_username);
                sb.AppendLine("APN_PASSWORD = " + _apn_password);
                sb.AppendLine("EPOCH_TIME_STAMP = " + _epoch_time_stamp);
                sb.AppendLine("SETTING_ID = " + _setting_id);
                sb.AppendLine("SENSORS_DEFINITION = " + _sensors_definition);
                
                if (_IsRTCSet == true)
                {
                    sb.AppendLine("RTC_SET = 1");
                }
                
                _sdStor.WriteToSettingFile(sb.ToString(), false);

                //send reply to hub
                //Create setting package
                string serial = _HW_VER.ToString() + StringPadleft(_HW_SERIAL.ToString(), '0', 5);
                string fw_ver = _FW_VER.ToString();
                string name = _DEVICE_NAME;
                string transmit_int = _TRANSMIT_INTERVAL.ToString();  //convert to minutes
                string ip = _ip;
                string port = _port;
                string sim_number = _SIMPhoneNo;
                string apn_name = _apn_name;
                string apn_username = _apn_username;
                string apn_password = _apn_password;
                string offline_threshold = _wireless_sensor_offline_threshold.ToString();
                string setting_id = _setting_id;
                string sensors_definition = _sensors_definition;
                StringBuilder sdsb = new StringBuilder(_sensors_definition);
                sdsb.Replace("DEVICE_SERIAL", serial);

                string setting = serial + "|" + fw_ver + "|" + name + "|" + transmit_int + "|" + offline_threshold + "|" + ip + "|" + port + "|" + sim_number + "|" + _apn_name + "|" + apn_username + "|" +
                                 apn_password + "|" + _epoch_time_stamp + "|" + _setting_id + "|" + sdsb.ToString().Trim() + "|";

                //CRC computation
                byte[] crc = Crc16.ComputeBytes(Encoding.UTF8.GetBytes(setting));
                byte[] reverse_crc = new byte[2];
                reverse_crc[0] = crc[1];
                reverse_crc[1] = crc[0];
                string crcStr = HTools.ByteArr.ToHex(reverse_crc);
                string settingStr = HTools.ByteArr.ToHex(Encoding.UTF8.GetBytes(setting));
                //Debug.Print(settingStr);

                //Xor with passphrase
                byte[] xor_setting_package_byte_array = util.PassPharseXOR(Encoding.UTF8.GetBytes(setting + crcStr), _xorPass);

                byte[] headerByte1 = Encoding.UTF8.GetBytes("DOK");
                byte[] headerByte2 = Encoding.UTF8.GetBytes("SET_SETTING");

                ArrayList settingByteArray = new ArrayList();
                foreach (byte b in headerByte1)
                {
                    settingByteArray.Add(new MyByte(b));
                }
                settingByteArray.Add(new MyByte(10));

                foreach (byte b in headerByte2)
                {
                    settingByteArray.Add(new MyByte(b));
                }
                settingByteArray.Add(new MyByte(10));

                foreach (byte b in xor_setting_package_byte_array)
                {
                    settingByteArray.Add(new MyByte(b));
                }

                byte[] sendByte = new byte[settingByteArray.Count + 1];
                MyByte myByte;
                for (int i = 0; i < settingByteArray.Count; i++)
                {
                    myByte = settingByteArray[i] as MyByte;
                    sendByte[i] = myByte.TheByte;
                }

                sendByte[sendByte.Length - 1] = 0x0A;

                ResetWatchDog();
                util.SendMsgOverTCPConnectionToHub(_sp3GModule, sendByte, false);
                ResetWatchDog();
               
                return;
            }
            catch
            {               
                return;
            }            
        }
        
        //COM3 send ack command for Point Six sensor to go to sleep
        public static void Send_PointSix_Sensor_ACK_CMD(string sensorSN)
        {
            if (sensorSN == null)
                return;

            if (String.Compare(sensorSN, "") == 0)
                return;

            if (_spCOM3.IsOpen == true)
            {
                HUtility util = new HUtility();
                util.WriteToUART(_spCOM3, HTools.Hex.ToBytes("C33C0020" + sensorSN.Trim() + "FFFFA6"), false);
                util = null;
            }
        } 
        
        //Get current wireless sensors reading
        public static string Get_Wireless_PointSix_Sensors_Reading()
        {
            StringBuilder sb = new StringBuilder();            
            foreach (var sensorObject in _SensorList)
            {
                if (sensorObject is SensorRecord)
                {
                    SensorRecord sensor = sensorObject as SensorRecord;
                    sb.AppendLine(sensor.TimeStamp.ToString() + ", " + sensor.SensorID + ", " + sensor.RawIOValue1.ToString() + ", " + sensor.RawIOValue2.ToString() + ", " + sensor.Battery.ToString());
                }
            }            
            return sb.ToString();
        }

        //Load settings from sd card
        public static void LoadSettingsFromSDCard()
        {
            HUtility util = new HUtility();   
            string[] settingsValue = _sdStor.ReadSettingFile();

            if (settingsValue == null)  //Error reading setting parameter from sd card, keep status led lit.
            {
                _statusLED.Write(true);
                SpecialGlobalVar._SystemNeedReboot = true;
                Thread.Sleep(Timeout.Infinite);     //wait for watchdog to reset the system
            }

            foreach (var item in settingsValue)
            {
                string[] keyValuePair = item.Split('=');
                if (keyValuePair.Length < 2)
                    continue;

                if (String.Compare("TRANSMIT_INTERVAL", keyValuePair[0].ToString().Trim()) == 0)
                {
                    //it is in minutes from settings file
                    _TRANSMIT_INTERVAL = int.Parse(keyValuePair[1].ToString().Trim());
                }
                else if (String.Compare("DEVICE_NAME", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _DEVICE_NAME = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("DEVICE_PHONE_NO", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _SIMPhoneNo = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("IP", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _ip = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("PORT", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _port = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("APN_NAME", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _apn_name = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("APN_USERNAME", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _apn_username = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("APN_PASSWORD", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _apn_password = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("WIRELESS_SENSOR_OFFLINE_THRESHOLD", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _wireless_sensor_offline_threshold = int.Parse(keyValuePair[1].ToString().Trim());
                }
                else if (String.Compare("EPOCH_TIME_STAMP", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _epoch_time_stamp = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("SETTING_ID", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _setting_id = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("SENSORS_DEFINITION", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _sensors_definition = keyValuePair[1].ToString().Trim();                    
                }                
                else
                {
                    continue;
                }
            }           
        }

        //Load incomplete download list if any
        public static void LoadIncompleteDownloadList()
        {
            _IncompleteDownloadList.Clear();
            string[] packages = _sdStor.ReadIncompleteDownloadListFromFile();

            if (packages == null)
                return;

            if (packages.Length <= 0)
                return;

            string[] data = null;
            foreach (var item in packages)
            {
                data = item.Split(',');
                if (data == null)
                    continue;
                if (data.Length != 15) //not the proper length, expect 15 data values
                    continue;
                
                _IncompleteDownloadList.Add(new IncompleteDownloadRecord(data[0], 
                                            new DateTime(Convert.ToInt32(data[1]), Convert.ToInt32(data[2]),Convert.ToInt32(data[3]),Convert.ToInt32(data[4]),Convert.ToInt32(data[5]),Convert.ToInt32(data[6])),
                                            new DateTime(Convert.ToInt32(data[7]), Convert.ToInt32(data[8]),Convert.ToInt32(data[9]),Convert.ToInt32(data[10]),Convert.ToInt32(data[11]),Convert.ToInt32(data[12])),                                          
                                            Convert.ToInt32(data[13]), Convert.ToInt32(data[14])));
            }
        }

        //Save any incomplete record to file
        public static void SaveIncompleteListToFile()
        {
            ResetWatchDog();

            //If the list already empty, delete the physical file
            if (_IncompleteDownloadList.Count == 0)
            {
                _IncompleteDownloadList.Clear();
                string rootDirectory = VolumeInfo.GetVolumes()[0].RootDirectory;
                //Check to see file exist, delete it 
                if (File.Exists(rootDirectory + @"\IncompleteDownloadList.txt") == true)
                {
                    File.Delete(rootDirectory + @"\IncompleteDownloadList.txt");
                }
                ResetWatchDog();
                return;
            }
               
            //Make sure the incomplete sensors list contain only registered sensors, if a sensor is not registered or old, remove it before saving    
            SensorRecord sr = null;            
            IncompleteDownloadRecord idr = null;
            ArrayList TempIncompleteDownloadRecord = new ArrayList();
           
            //Get only the sensor that are registered to a temporary list            
            for (int i = 0; i < _IncompleteDownloadList.Count; i++)
            {
                idr = _IncompleteDownloadList[i] as IncompleteDownloadRecord;

                for (int j = 0; j < _SensorList.Count; j++)
                {                    
                    //Check to see if it's a wireless sensor object (Point Six sensor)
                    if (_SensorList[j] is SensorRecord)
                    {
                        sr = _SensorList[j] as SensorRecord;
                        if (String.Compare(idr.SensorID.Trim(), sr.SensorID.Trim()) == 0)
                        {
                            //Add to temporary list
                            TempIncompleteDownloadRecord.Add(idr);
                        }
                    }
                }                         
            }

            //If TempIncompleteDownloadRecord is empty, delete the incomplete text file else write to file
            if (TempIncompleteDownloadRecord.Count > 0)
            {
                _IncompleteDownloadList.Clear();
                for (int i = 0; i < TempIncompleteDownloadRecord.Count; i++)
                {
                    _IncompleteDownloadList.Add(TempIncompleteDownloadRecord[i]);
                }

                ResetWatchDog();

                _sdStor.WriteIncompleteDownloadListToFile(_IncompleteDownloadList);

                ResetWatchDog();
            }            
            
        }

        /// <summary>
        /// Update the database of sensors according to the sensors definition given
        /// </summary>
        /// <param name="sensorsDefinition"></param>
        public static void RegisterWiredAndWirelessSensorsConfiguration(string sensorsDefinition)
        {
            _SensorList.Clear();
            HUtility util = new HUtility();
            try
            {                
                string[] sensorList = sensorsDefinition.Split(new char[] { '$' });
                int count = 1;
                foreach (var sensor in sensorList)
                {
                    string[] sensorDetail = sensor.Split(new char[] { '*' });
                    if (String.Compare(sensorDetail[0].Trim(), "ps") == 0)
                    {
                        string[] ioDetail = sensorDetail[4].Split(new char[] { ',' });
                        if (ioDetail.Length == 2)
                        {
                            _SensorList.Add(new SensorRecord(sensorDetail[3].Trim(), 0, RealTimeClock.GetDateTime(), 0, 0, "N/A", 104, 0, -1, -1, -1, 3, count));
                            count++;
                        }
                        else if (ioDetail.Length == 1)
                        {
                            string[] ioDetail1 = ioDetail[0].Split(new char[] { ':' });
                            if (String.Compare(ioDetail1[0].Trim(), "io1") == 0)
                            {
                                _SensorList.Add(new SensorRecord(sensorDetail[3].Trim(), 0, RealTimeClock.GetDateTime(), 0, 0, "N/A", 104, 0, -1, -1, -1, 1, count));
                                count++;
                            }
                            else
                            {
                                _SensorList.Add(new SensorRecord(sensorDetail[3].Trim(), 0, RealTimeClock.GetDateTime(), 0, 0, "N/A", 104, 0, -1, -1, -1, 2, count));
                                count++;
                            }
                        }
                        else
                        {
                            //Error in sensors' configuration
                        }
                    }
                    else if (String.Compare(sensorDetail[0].Trim(), "gprs") == 0)
                    {
                        string[] gprsIOList = sensorDetail[4].Split(new char[] { ',' });
                        for (int i = 0; i < gprsIOList.Length; i++)
                        {
                            string[] ioDetail = gprsIOList[i].Split(new char[] { ':' });
                            //add to _registeredWiredPorts array list the gprs port
                            _SensorList.Add(new GPRSPort(ioDetail[0].Trim(), count));
                            count++;
                        }
                    }
                    else
                    {
                        //ignore the damn thing
                    }
                }

                //Check to see if there are any sensors in the list first, enter sensors data into database if it is not empty
                if (_SensorList.Count <= 0)
                    return;
                
                //Insert to SQLite Database, important we must insert with index so when it loads, the sensor defintion will match with the _SensorList
                //right now the _SensorList is match with the sensor definition
                DB db = new DB();
                SensorRecord sr = null;
                GPRSPort port_io = null;
                if (db.OpenDB() == true)
                {
                    //Clear all tables
                    if (db.TruncateDBTables() == true)
                    {                        
                        foreach (var sensor in _SensorList)
                        {
                            if (sensor is SensorRecord)
                            {
                                sr = sensor as SensorRecord;
                                db.InsertSensorRecord(sr, sr.DefIndex);
                            }
                            else if (sensor is GPRSPort)
                            {
                                port_io = sensor as GPRSPort;
                                db.InsertPortRecord(port_io, port_io.DefIndex);
                            }                            
                        }
                    }
                }
                db.CloseDB();                  
            }
            catch
            {
                //Need system reset
                SpecialGlobalVar._SystemNeedReboot = true;

                while (true)
                {
                    util.BlinkLED(_statusLED, 1, 300, LED_Blink_Type.ON_OFF);
                }
            }
        }

        public static void TransmitDeviceDataPackage()
        {            
            HUtility util = new HUtility();

            /////////////////////////Test Code////////////////
            //DateTime rtc = RealTimeClock.GetDateTime();
            //DateTime systemTime = DateTime.Now;

            //util.WriteToUART(_sp3GModule, "RTC_SET: " + _IsRTCSet.ToString(), true);
            //util.WriteToUART(_sp3GModule, "RTC: " + rtc.ToString(), true);
            //util.WriteToUART(_sp3GModule, "System Time: " + systemTime.ToString(), true);
            //////////////////////////////////////////////////


            byte[] byte_dataPackage = new byte[] { };
            //Get IOs readings 
            IOData ioData = GetIOReadings();

            //Create data package to send
            lock (_GPSDataObject)
            {
                //Get byte package containing wired and wireless data according to the configuration
                byte_dataPackage = CreateBytesPackageToSend(ioData);
            }

            if (_3GOnline)
            {                
                //Open TCP Session and send message
                if (util.OpenTCPSession(_sp3GModule, _ip, _port) == true)
                {
                    //Successfully connected to the hub through TCP, reset "SpecialGlobalVar._TCPConnectionRetries" variable
                    SpecialGlobalVar._TCPConnectionRetries = 0;

                    /////////////////////////////////////////////////////////////////////////////////
                    //If system just reset or reboot by the Watchdog timer, send setting to HUB
                    //
                    //put watch dog code here to detect watchdog just reset the system                    
                    /////////////////////////////////////////////////////////////////////////////////


                    ////////////////////////////
                    ////Test code
                    //string psSensors = Get_Wireless_PointSix_Sensors_Reading();
                    //util.SendMsgOverTCPConnection(_spGSM, psSensors);
                    //////////////////////////////////////////////////////


                    //Turn on transmission LED
                    _transmissionLED.Write(true);                    

                    //Send data over TCP Connection to HUB                       
                    ResetWatchDog();
                    HUB_RESPONSE hub_response = util.SendMsgOverTCPConnectionToHub(_sp3GModule, byte_dataPackage, 1000, 20, true);
                    ResetWatchDog();

                    //No response from HUB, store undeliverable data back to files
                    if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.NO_RESPONSE_3G_MODULE_TIMEOUT)
                    {
                        //Record the package that was failed to deliver                           
                        ArrayList dataPackageHexStr = new ArrayList();
                        dataPackageHexStr.Add(util.ByteArrayToHexString(byte_dataPackage));
                        _sdStor.WriteUndeliverableDataToDataLogFile(dataPackageHexStr, _LogFileIndex, out _LogFileIndex, MAX_LOG_FILE_SIZE, LOG_FILE_TYPE.REGULAR_LOG_FILE);

                        //Close the TCP session                   
                        util.CloseTCPSession(_sp3GModule);

                        //Turn off transmission LED
                        _transmissionLED.Write(false);
                        
                        return;
                    }

                    if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.NOT_OK || hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.OK_WITH_NO_ACTION_COMMAND)
                    {
                        //ignore, don't store undeliverable data back to files in the case of NOT OK                        
                        //upload history log or download log if any, during uploading history log or download log, device will not handle
                        //request setting and set setting from host
                        UploadHistoryLog(LOG_FILE_TYPE.REGULAR_LOG_FILE);
                        UploadHistoryLog(LOG_FILE_TYPE.SENSOR_DOWNLOAD_LOG_FILE);

                        //Close the TCP session                   
                        util.CloseTCPSession(_sp3GModule);

                        //Turn off transmission LED
                        _transmissionLED.Write(false);                       

                        return;
                    }
                                                            
                    if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.OK_WITH_REQUEST_SETTING)  //Hub response OK with requesting device settings
                    {
                        //Handle HUB request for Setting
                        hub_response = HandleHubRequestForSetting();                        
                    }

                    if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.OK_WITH_SET_SETTING) //Hub response OK with requesting device to set its settings according to the Hub payload
                    {
                        //Set settings according to Hub
                        HandleHubCommandForSetSetting(hub_response.ResponseByteArray);
                    }
                                                          
                    //Close the TCP session                   
                    util.CloseTCPSession(_sp3GModule);
                                       
                    //Turn off transmission LED
                    _transmissionLED.Write(false);                                 
                }
                else
                {
                    //keep track of how many failed attempts so we can hard reset the 3G module
                    SpecialGlobalVar._TCPConnectionRetries++;

                    //try to close the TCP session                        
                    util.CloseTCPSession(_sp3GModule);
                   
                    //Record the package that was failed to deliver                           
                    ArrayList dataPackageHexStr = new ArrayList();
                    dataPackageHexStr.Add(util.ByteArrayToHexString(byte_dataPackage));
                    _sdStor.WriteUndeliverableDataToDataLogFile(dataPackageHexStr, _LogFileIndex, out _LogFileIndex, MAX_LOG_FILE_SIZE, LOG_FILE_TYPE.REGULAR_LOG_FILE);                    
                }

                Debug.GC(true);
            }
            
        }


        #endregion



        /////////////////////////////////////////////
        // PointSix Wireless Sensor Helper Methods //
        /////////////////////////////////////////////

        #region  [PointSix Wireless Sensor Helper Methods]

        public static void DownloadHistoryRecordsAndSendItToHub(SensorRecord beaconRecord, int[] memoryBlockList, DateTime startDate, DateTime endDate)
        {
            if (memoryBlockList == null)
                return;

            string[] historyPackageList = new String[memoryBlockList.Length];
            int historyPackageCount = 0;
            int lastAcquiredMemoryBlockListIndex = -1;        //keep track of the last memory block successfully acquired from the sensor so that we can determine what remaining
            string memLocation = null;
            string sensorSN = beaconRecord.SensorID;
            HUtility util = new HUtility();

            _stopWaiting = false;

            //Get the date and time of the start of the download for later use in parsing process.  This determine the time adjustment
            _sensorDownloadDateTime = DateTime.Now;

            //Start requesting memory blocks from the list  
            for (int i = 0; i < memoryBlockList.Length; i++)
            {
                if (_stopWaiting == true)     //Stop getting data, no reponse from sensor for the waited interval
                {
                    break;
                }

                //Memory location to get package                                       
                memLocation = memoryBlockList[i].ToString("X");

                //Send Get Log Data to sensor                                          
                _sendPacket = HTools.Hex.ToBytes(_REQ_DATA_CMD + sensorSN + "FF" + "00" + memLocation);
                util.WriteToUART(_spCOM3, _sendPacket, false);

                //Wait until you get the right data block back
                _ps_download_count_1 = 0;
                _ps_download_count_2 = 0;

                while (true)
                {
                    Thread.Sleep(1);
                    _ps_download_count_1++;
                    _ps_download_count_2++;

                    if (_ps_download_count_2 > 3000)
                    {
                        _stopWaiting = true;
                        break;
                    }


                    if (_NewPointSixPackageReceived)  //If there is a new package received
                    {
                        //SpecialGlobalVar._XBEE900SerialBuffer.CopyTo(_PointSixPackage, 0);
                        _NewPointSixPackageReceived = false;  //reset flag

                        string hexPackage = HTools.ByteArr.ToHex(SpecialGlobalVar._XBEE900SerialBuffer).ToUpper();

                        //Check to see if it is a Sensor Log Data Response                                          
                        if (hexPackage.Substring(6, 2) == "22")
                        {
                            //Check for match serial number, transaction id, and requested memory location                            
                            if (hexPackage.Substring(16, 2) == "FF" && hexPackage.Substring(8, 8) == _parsedSensorRecord.SensorID && hexPackage.Substring(20, 2) == memLocation)
                            {
                                //Add to dataList
                                historyPackageList[historyPackageCount] = HTools.ByteArr.ToHex(SpecialGlobalVar._XBEE900SerialBuffer);
                                historyPackageCount++;
                                lastAcquiredMemoryBlockListIndex = i;

                                ////////////////////////Test Code //////////////////////////

                                //Debug.Print("History: " + hexPackage);

                                ////////////////////////////////////////////////////////////

                                break;
                            }
                        }
                    }

                    if (_ps_download_count_1 > 200)  //Resend command if requested package haven't comes in for a period of time
                    {
                        util.WriteToUART(_spCOM3, _sendPacket, false);
                        _ps_download_count_1 = 0;
                    }

                }//End while loop 

            }//End For Loop


            if (lastAcquiredMemoryBlockListIndex == (memoryBlockList.Length - 1))
            {
                //tell sensor to go to sleep, we got all required memory blocks
                Send_PointSix_Sensor_ACK_CMD(_parsedSensorRecord.SensorID, 3);

                
                ////////////////////////////Test Code ///////////////////////////////
                //Debug.Print("All memory blocks downloaded successfully.");
                //SensorRecord srTest = null;
                //Debug.Print("---------------------------------REGISTERED SENSORS LIST---------------------------");
                //foreach (var sensor in _SensorList)
                //{
                //    srTest = sensor as SensorRecord;
                //    if (srTest != null)
                //        Debug.Print(srTest.ToString() + "\n");
                //}

                //Debug.Print("---------------------------------END LISTS-----------------------------------------");
                ////////////////////////////////////////////////////////////////////////
            }
            else
            {
                //Make sure the incomplete list doesn't contain

                int startMemBlock = memoryBlockList[lastAcquiredMemoryBlockListIndex + 1];
                int endMemBlock = memoryBlockList[memoryBlockList.Length - 1];
                _IncompleteDownloadList.Add(new IncompleteDownloadRecord(sensorSN, startDate, endDate, startMemBlock, endMemBlock));

                /////////////////////Test Code//////////////////////////////
                //string rem_mem = null;
                //for (int i = lastAcquiredMemoryBlockListIndex + 1; i < memoryBlockList.Length; i++)
                //{
                //    rem_mem += memoryBlockList[i].ToString("X") + ", ";
                //}
                //Debug.Print("Remaining memory blocks: " + rem_mem);

                //IncompleteDownloadRecord ir = null;
                //Debug.Print("---------------------------------INCOMPLETE SENSORS LIST---------------------------");
                //foreach (var record in _IncompleteDownloadList)
                //{
                //    ir = record as IncompleteDownloadRecord;
                //    if (ir != null)
                //        Debug.Print(ir.ToString() + "\n");
                //}

                //Debug.Print("---------------------------------END INCOMPLETE LIST-------------------------------");

                //SensorRecord srTest = null;
                //Debug.Print("---------------------------------REGISTERED SENSORS LIST---------------------------");
                //foreach (var sensor in _SensorList)
                //{
                //    srTest = sensor as SensorRecord;
                //    if (srTest != null)
                //        Debug.Print(srTest.ToString() + "\n");
                //}

                //Debug.Print("---------------------------------END LISTS-----------------------------------------");


                /////////////////////////////////////////////////////////////
            }

            //Parse the downloaded packages into SensorRecord object Queue
            Queue ParsedRecordQueue = ParseDownloadPackages(historyPackageList, startDate, endDate, beaconRecord);
            

            //Start sending download 
            if (ParsedRecordQueue != null)
            {
                if (ParsedRecordQueue.Count > 0)
                {
                    //Parse the downloaded packages into hex string to create Download Data Package
                    Queue DownloadDataPackageQueue = CreateDownloadLogBytePackagesForPointSixSensor(ParsedRecordQueue, 24);

                    //Wait until GSM unit is free
                    while (SpecialGlobalVar._3GModuleSerialPortBusy == true)
                    { }
                    
                    SpecialGlobalVar._3GModuleSerialPortBusy = true;  //set flag

                    SendDownloadSensorPackage(DownloadDataPackageQueue);

                    SpecialGlobalVar._3GModuleSerialPortBusy = false; //reset flag
                }
            }

            //Set variables to null for garbage collector            
            util = null;
            memLocation = null;
            Array.Clear(memoryBlockList, 0, memoryBlockList.Length);
            memoryBlockList = null;
            Array.Clear(historyPackageList, 0, historyPackageList.Length);
            historyPackageList = null;

            Debug.GC(true);
        }

        public static void SendDownloadSensorPackage(Queue downloadDataPackageQueue)
        {            
            if (_3GOnline == true)
            {                
                HUtility util = new HUtility();
                              
                //Open TCP Session and send message
                if (util.OpenTCPSession(_sp3GModule, _ip, _port) == true)
                {
                    //Successfully connected to the hub through TCP, reset "SpecialGlobalVar._TCPConnectionRetries" variable
                    SpecialGlobalVar._TCPConnectionRetries = 0;
                   
                    //Turn on transmission LED
                    _transmissionLED.Write(true);


                    HUB_RESPONSE hub_response = new HUB_RESPONSE(HUB_RESPONSE_CODE.NOT_OK, null);
                    ArrayList failedSendPackageBytesStringList = new ArrayList();
                    byte[] byte_dataPackage = null;
                    string currentPackageBytesString = null;
                    //Begin sending data over TCP Connection to HUB                       
                    while (downloadDataPackageQueue.Count > 0)
                    {
                        //Get the package byte string
                        currentPackageBytesString = downloadDataPackageQueue.Dequeue() as string;
                        //Convert package byte string to byte array
                        byte_dataPackage = util.HexStringToByteArray(currentPackageBytesString);

                        //Send the byte array package
                        hub_response = util.SendMsgOverTCPConnectionToHub(_sp3GModule, byte_dataPackage, 1000, 20, false);                       
                      
                        //Failed send or no response from HUB, store package back onto a queue and exit loop
                        if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.NO_RESPONSE_3G_MODULE_TIMEOUT ||
                            hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.NO_RESPONSE_3G_MODULE_DID_NOT_TIMEOUT)
                        {
                            //Record the package that was failed to deliver to the queue, record the byte string not the byte array                           
                            failedSendPackageBytesStringList.Add(currentPackageBytesString);

                            //Check to see if there are any packages that were not send, must store to SD Card
                            if (failedSendPackageBytesStringList.Count > 0)
                            {
                                _sdStor.WriteUndeliverableDataToDataLogFile(failedSendPackageBytesStringList, _DownloadLogFileIndex, out _DownloadLogFileIndex, MAX_LOG_FILE_SIZE, LOG_FILE_TYPE.SENSOR_DOWNLOAD_LOG_FILE);
                            }

                            //Check to see if the queue still have any package left, must store into a file as well
                            if (downloadDataPackageQueue.Count > 0)
                            {
                                ArrayList leftOverPackageList = new ArrayList();
                                for (int i = 0; i < downloadDataPackageQueue.Count; i++)
                                {
                                    leftOverPackageList.Add(downloadDataPackageQueue.Dequeue());
                                }

                                _sdStor.WriteUndeliverableDataToDataLogFile(leftOverPackageList, _DownloadLogFileIndex, out _DownloadLogFileIndex, MAX_LOG_FILE_SIZE, LOG_FILE_TYPE.SENSOR_DOWNLOAD_LOG_FILE);
                            }

                            return;
                        }

                        //OK response from HUB with no Action Command, continue upload from Queue    
                        if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.OK_WITH_NO_ACTION_COMMAND)   
                        {
                            continue;
                        }

                        //Hub response OK with requesting device settings, handle the request then continue
                        if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.OK_WITH_REQUEST_SETTING)  
                        {
                            //Handle HUB request for Setting
                            HandleHubRequestForSetting();
                            continue;
                        }

                        //Hub response OK with requesting device to set its settings according to the Hub payload, handle request then continue
                        if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.OK_WITH_SET_SETTING) 
                        {
                            //Set settings according to Hub
                            HandleHubCommandForSetSetting(hub_response.ResponseByteArray);
                            continue;
                        }

                        //Ignore, don't store undeliverable data back onto a queue and continue;                        
                        if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.NOT_OK  || hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.INVALID_DATA)
                        {
                            continue;
                        }

                    }//end while loop

                    //set variable to null for GC to collect
                    hub_response = null;

                    //Close the TCP session
                    util.CloseTCPSession(_sp3GModule);
                    
                    //Turn off transmission LED
                    _transmissionLED.Write(false);
                    
                }
                else
                {
                    //keep track of how many failed attempts so we can hard reset 3G module
                    SpecialGlobalVar._TCPConnectionRetries++;

                    //try to close the TCP session                        
                    util.CloseTCPSession(_sp3GModule);                    

                    //Check to see if the queue still have any packages, must store to SD Card
                    if (downloadDataPackageQueue.Count > 0)
                    {
                        ArrayList leftOverPackageList = new ArrayList();
                        for (int i = 0; i < downloadDataPackageQueue.Count; i++)
                        {
                            leftOverPackageList.Add(downloadDataPackageQueue.Dequeue());
                        }

                        _sdStor.WriteUndeliverableDataToDataLogFile(leftOverPackageList, _DownloadLogFileIndex, out _DownloadLogFileIndex, MAX_LOG_FILE_SIZE, LOG_FILE_TYPE.SENSOR_DOWNLOAD_LOG_FILE);
                    }                                           
                }

                Debug.GC(true);
            } 


        }

        public static SensorRecord ParsePackage(byte[] byteData)
        {
            try
            {
                HUtility util = new HUtility();
                byte[] sanitizedBytes = null;
                string serialNo = GetSensorSerialNumber(byteData);
                if (serialNo == null)
                    return null;

                //Get sensor type here...
                byte[] sType = new byte[2];
                for (int i = 0; i < 2; i++)
                {
                    sType[i] = byteData[i + 34];
                }
                sanitizedBytes = util.SanitizeForUTF8(sType);                
                string sensorTypeCode = new string(Encoding.UTF8.GetChars(sanitizedBytes, 0, sanitizedBytes.Length));


                DateTime currentDateTime = DateTime.Now;
                //Get sensor clock
                byte[] sClk = new byte[4];
                sClk[0] = byteData[24]; sClk[1] = byteData[25]; sClk[2] = byteData[26]; sClk[3] = byteData[27];
                int sensorClk = Convert.ToInt32(HTools.ByteArr.ToHex(sClk), 16);
                _timeAdjustment = currentDateTime.AddSeconds(sensorClk * (-1));


                //Get the two IOs, they store opposite for RTD single and RTD dual.  First analog value is channel 2 and second analog value is channel 1
                int ioValue1 = 0, ioValue2 = 0;
                byte[] IOValue1ByteArray = new byte[4];
                byte[] IOValue2ByteArray = new byte[4];
                for (int i = 0; i < 4; i++)
                {
                    IOValue2ByteArray[i] = byteData[i + 48];
                }
                for (int i = 0; i < 4; i++)
                {
                    IOValue1ByteArray[i] = byteData[i + 52];
                }
                sanitizedBytes = util.SanitizedForValidHexBytes(IOValue1ByteArray);
                if (sanitizedBytes == null)
                    return null;

                string ioValue1HexStr = new string(Encoding.UTF8.GetChars(sanitizedBytes, 0, sanitizedBytes.Length));
                ioValue1 = Convert.ToInt32(ioValue1HexStr, 16);

                sanitizedBytes = util.SanitizedForValidHexBytes(IOValue2ByteArray);
                if (sanitizedBytes == null)
                    return null;

                string ioValue2HexStr = new string(Encoding.UTF8.GetChars(sanitizedBytes, 0, sanitizedBytes.Length));
                ioValue2 = Convert.ToInt32(ioValue2HexStr, 16);

                //Get Alarm status
                int alarmStatus = Int32.Parse(byteData[72].ToString());

                ////Get package origin
                //string packageOrigin = GetPackageOrigin(byteData);

                //Get the transmit period in seconds
                byte[] byteTransmitPeriod = new byte[2];
                for (int i = 0; i < 2; i++)
                {
                    byteTransmitPeriod[i] = byteData[i + 70];
                }
                string transmitPeriodHexStr = HTools.ByteArr.ToHex(byteTransmitPeriod);
                int transmitPeriod = Convert.ToInt32(transmitPeriodHexStr, 16);

                //Get battery life            
                byte[] byteBattPktCnt = new byte[3];
                byte[] byteMaxBattPktCnt = new byte[3];
                for (int i = 0; i < 3; i++)
                {
                    byteBattPktCnt[i] = byteData[i + 64];
                    byteMaxBattPktCnt[i] = byteData[i + 67];
                }
                string byteBattPktCntHexStr = HTools.ByteArr.ToHex(byteBattPktCnt);
                string byteMaxBattPktCntHexStr = HTools.ByteArr.ToHex(byteMaxBattPktCnt);
                double BattPktCnt = Convert.ToInt32(byteBattPktCntHexStr, 16);
                double MaxBattPktCnt = Convert.ToInt32(byteMaxBattPktCntHexStr, 16);
                int batteryLife = 0;

                //Check to see if battery usage information is supported
                if (transmitPeriod == 0 && MaxBattPktCnt == 0)
                {
                    batteryLife = 102; //Battery is not supported by this sensor
                }
                else
                {
                    batteryLife = (int)(100 - ((BattPktCnt / MaxBattPktCnt) * 100));
                    //Check to see if Battery life is within 0-100%
                    if (batteryLife < 0 || batteryLife > 100)
                        batteryLife = 101;
                }

                //Get the Block Index and Record Index
                int blockIndex = -1, recordIndex = -1;
                GetBlockIndexAndRecordIndex(byteData, out blockIndex, out recordIndex);

                return new SensorRecord(serialNo, transmitPeriod, currentDateTime, ioValue1, ioValue2, sensorTypeCode, batteryLife, alarmStatus, blockIndex, recordIndex, sensorClk, 0, -1);
            }
            catch
            {
                //Swallow exception               
                Debug.GC(true);
                return null;
            }
        }

        public static Queue ParseDownloadPackages(string[] historyPackageList, DateTime startDate, DateTime endDate, SensorRecord beaconRecord)
        {           
            Queue parsedRecordQueue = new Queue();
            //Get Sensor Clock
            _timeAdjustment = _sensorDownloadDateTime.AddSeconds(beaconRecord.SensorClock * (-1));

            //Find the sensor, get the ioFlag                          
            int ioflag = 0;
            for (int i = 0; i < _SensorList.Count; i++)
            {
                //Check to see if it's a wireless sensor object, it is a PointSix sensor
                if (_SensorList[i] is SensorRecord)
                {
                    SensorRecord sr = _SensorList[i] as SensorRecord;
                    if (String.Compare(sr.SensorID, beaconRecord.SensorID) == 0)
                    {
                        ioflag = sr.IOFlag;
                        break;
                    }
                }
            }

            int timeHeader = 0, historyLogTimeOffset = 0, timeStamp = 0;
            int datacount = 0, ioValue1 = 0, ioValue2 = 0;
            string[] byteStrArray = new string[75];
            foreach (var dataStr in historyPackageList)
            {
                if (dataStr == null)        //string is null, continue
                    continue;
                if (String.Compare(dataStr, "") == 0)
                    continue;
                if (dataStr.Length != 150)  //does not contain 150 characters, continue
                    continue;

                datacount++;

                int incr = 0;
                for (int i = 0; i < 75; i++)
                {
                    byteStrArray[i] = dataStr.Substring(incr, 2).ToUpper();
                    incr = incr + 2;
                }

                //Check timeHeader to verify if there are data in the current block, continue, else get time header
                if (byteStrArray[11] == "FF")
                    continue;

                timeHeader = Convert.ToInt32(byteStrArray[11] + byteStrArray[12] + byteStrArray[13] + byteStrArray[14], 16);
                if (timeHeader == 6945450)
                    timeStamp = 0;
                else
                    timeStamp = 0;

                ////////////////////////////////////
                //  Start getting history records //
                ////////////////////////////////////

                //Go through record index for each memory block                   
                int indexCount = 0;
                //bool foundMatchDT = false;
                DateTime currentRecordTimeStamp;
                for (int i = 0; i < 12; i++)
                {
                    //Check if it is an alarm record, if so ignore
                    int isAlarm = Convert.ToInt32(byteStrArray[15 + indexCount], 16);
                    isAlarm = isAlarm & 1;
                    if (isAlarm == 1)
                    {
                        indexCount += 5;
                        continue;
                    }

                    //Get current record time stamp
                    historyLogTimeOffset = Convert.ToInt32(byteStrArray[15 + indexCount], 16) * 30;
                    timeStamp += historyLogTimeOffset;
                    currentRecordTimeStamp = _timeAdjustment.AddSeconds(timeHeader + timeStamp);

                    //Get ioValue1 and ioValue2
                    ioValue1 = Convert.ToInt32(byteStrArray[16 + indexCount] + byteStrArray[17 + indexCount], 16);
                    ioValue2 = Convert.ToInt32(byteStrArray[18 + indexCount] + byteStrArray[19 + indexCount], 16);

                    //add only record that is between start and end date timeline
                    if (currentRecordTimeStamp >= startDate && currentRecordTimeStamp < endDate)
                    {
                        parsedRecordQueue.Enqueue(new SensorRecord(beaconRecord.SensorID, historyLogTimeOffset, currentRecordTimeStamp, ioValue1, ioValue2, beaconRecord.Type,
                                                                  0, 0, 0, 0, beaconRecord.SensorClock, ioflag, -1));
                    }

                    indexCount += 5;
                }
            }

            return parsedRecordQueue;
        }

        public static string GetSensorSerialNumber(byte[] byteData)
        {
            try
            {
                //Get Sensor Serial Number
                HUtility util = new HUtility();
                byte[] sanitizedBytes = null;
                byte[] SensorSN = new byte[8];
                for (int i = 0; i < 8; i++)
                { SensorSN[i] = byteData[i + 36]; }
                sanitizedBytes = util.SanitizePointSixSerialNoByteArray(SensorSN);

                if (sanitizedBytes == null) //invalid serial number
                    return null;

                string serialNo = new string(Encoding.UTF8.GetChars(sanitizedBytes, 0, sanitizedBytes.Length));
                if (serialNo == null)
                    return null;
                if (serialNo.Length < 8)
                    return null;

                return serialNo.Trim();
            }
            catch
            {
                return null;
            }
        }

        public static void Send_PointSix_Sensor_ACK_CMD(string sensorSN, int repeat)
        {
            if (sensorSN == null)
                return;

            if (String.Compare(sensorSN, "") == 0)
                return;

            if (_spCOM3.IsOpen == true)
            {
                HUtility util = new HUtility();
                for (int i = 0; i < repeat; i++)
                {
                    util.WriteToUART(_spCOM3, HTools.Hex.ToBytes("C33C0020" + sensorSN.Trim() + "FFFFA6"), false);
                    Thread.Sleep(200);
                }
                util = null;
            }
        }

        public static void GetBlockIndexAndRecordIndex(byte[] logNextPointer, out int blockIndex, out int recordIndex)
        {
            //Pre-defined by Point Six manufacture, formula for getting block index and the next record index in the block is
            //  iBlock = Log Next/LOGBLOCKSIZE
            //  iRecord = ((Log Next % LOGBLOCKSIZE) - LOGBLOCKTIMEHEADERSIZE) / LOGPACKETSIZE
            //Log Next should be masked with 3FFF before usage

            int LOGBLOCKSIZE = 64;      // 64 Bytes
            int LOGBLOCKTIMEHEADERSIZE = 4;     // 4 Bytes
            int LOGPACKETSIZE = 5;       //5 Bytes

            //Get the "Log Next Pointer"   
            byte[] logNP = new byte[2];
            logNP[0] = logNextPointer[28];
            logNP[1] = logNextPointer[29];
            string logNPHexString = HTools.ByteArr.ToHex(logNP);


            //Check for End of Memory Transistion
            if (String.Compare(logNPHexString, "4000") == 0)    // Pointing to the first block in the log, just wrapped
            {
                blockIndex = 0;
                recordIndex = 0;
                return;
            }

            //Mask "Log Next Pointer" with 3FFF
            int logNextDecimal = Convert.ToInt32(logNPHexString, 16);
            int maskValue = Convert.ToInt32("3FFF", 16);
            int logNextValue = logNextDecimal & maskValue;

            blockIndex = logNextValue / LOGBLOCKSIZE;
            recordIndex = ((logNextValue % LOGBLOCKSIZE) - LOGBLOCKTIMEHEADERSIZE) / LOGPACKETSIZE;

        }

        public static string GetPackageOrigin(byte[] bytesArray)
        {
            HUtility util = new HUtility();
            //Get package origin, who sent this package?  0-Wifi, 1-Point Manager, 2-Ethernet Point Repeater, 3-Application, 4-Wifi Repeater, 5-Counter Manager, 6-Wifi Sensor PassThru(Point Manager),
            //7-Link Manager, 8-Wifi Repeater Logger, 9-Wifi Repeater Alarm
            string packageOrigin = "";
            string abyte = bytesArray[63].ToString();
            int packageOriginCode = Int32.Parse(abyte);


            switch (packageOriginCode)
            {
                case 0:
                    packageOrigin = "WiFi/Direct from Sensor";
                    break;
                case 1:
                    packageOrigin = "Point Manager";
                    break;
                case 2:
                    packageOrigin = "Ethernet Point Repeater";
                    break;
                case 3:
                    packageOrigin = "Application";
                    break;
                case 4:
                    packageOrigin = "WiFi Repeater";
                    break;
                case 5:
                    packageOrigin = "Counter Manager";
                    break;
                case 6:
                    packageOrigin = "WiFi Sensor PassThru(Point Manager)";
                    break;
                case 7:
                    packageOrigin = "Link Manager";
                    break;
                case 8:
                    packageOrigin = "WiFi Repeater Logger";
                    break;
                case 9:
                    packageOrigin = "WiFi Repeater Alarm";
                    break;
                default:
                    packageOrigin = "Unknown";
                    break;
            }
            return packageOrigin;
        }

        public static ArrayList GetMemoryList(int startBlock, int endBlock)
        {
            ArrayList memList = new ArrayList();

            //Base cases
            if (startBlock == endBlock)
            {
                memList.Add(startBlock);
                return memList;
            }

            for (int i = startBlock; i <= endBlock; i++)
            {
                memList.Add(i);
            }

            return memList;
        }

        public static int[] GetMemoryListForDownload(SensorRecord newRecord, SensorRecord oldRecord, int oldestRecord)
        {
            //Current sensor clock is less than previous sensor clock, must request for oldest log record and collect from there, if this is the case oldestRecord is not -1    
            if (oldestRecord > -1)  //must download from oldest to current block
            {
                //If oldest record is FF=255, then just add it to the beginning of the list, then add block 0 to current block
                if (oldestRecord == 255)
                {
                    ArrayList memArray = GetMemoryList(0, newRecord.BlockIndex);
                    memArray.Add(255);
                    Array memList = Array.CreateInstance(typeof(int), memArray.Count);
                    memArray.CopyTo(memList, 0);
                    return (int[])memList;
                }
                else if (oldestRecord > newRecord.BlockIndex)   //we must create two list, one from oldest record to 255, and another from 0 to current block
                {
                    ArrayList memArray1 = GetMemoryList(oldestRecord, 255);
                    ArrayList memArray2 = GetMemoryList(0, newRecord.BlockIndex);
                    Array memList = Array.CreateInstance(typeof(int), memArray1.Count + memArray2.Count);
                    memArray1.CopyTo(memList, 0);
                    memArray2.CopyTo(memList, memArray1.Count);
                    return (int[])memList;
                }
                else //this is when the oldest record is less then the current record memory block
                {
                    ArrayList memArray = GetMemoryList(oldestRecord, newRecord.BlockIndex);
                    Array memList = Array.CreateInstance(typeof(int), memArray.Count);
                    memArray.CopyTo(memList, 0);
                    return (int[])memList;
                }
            }

            //Memory rolled over, generate 2 sequences, from sr.BlockIndex to FF = 255 block, and from 0 to current block index
            if (oldRecord.BlockIndex > newRecord.BlockIndex)
            {
                ArrayList memArray1 = GetMemoryList(oldRecord.BlockIndex, 255);
                ArrayList memArray2 = GetMemoryList(0, newRecord.BlockIndex);
                Array memList = Array.CreateInstance(typeof(int), memArray1.Count + memArray2.Count);
                memArray1.CopyTo(memList, 0);
                memArray2.CopyTo(memList, memArray1.Count);
                return (int[])memList;
            }
            else //Same block index or normal case            
            {
                ArrayList memArray = GetMemoryList(oldRecord.BlockIndex, newRecord.BlockIndex);
                Array memList = Array.CreateInstance(typeof(int), memArray.Count);
                memArray.CopyTo(memList, 0);
                return (int[])memList;
            }
        }

        public static void UpdateSensorRecord(SensorRecord newRecord, SensorRecord oldRecord)
        {
            try
            {
                oldRecord.TimeStamp = DateTime.Now;
                oldRecord.RawIOValue1 = newRecord.RawIOValue1;
                oldRecord.RawIOValue2 = newRecord.RawIOValue2;
                oldRecord.SensorClock = newRecord.SensorClock;
                oldRecord.TransmitPeriod = newRecord.TransmitPeriod;
                oldRecord.BlockIndex = newRecord.BlockIndex;
                oldRecord.RecordIndex = newRecord.RecordIndex;
                oldRecord.Battery = newRecord.Battery;
                oldRecord.AlarmStatus = newRecord.AlarmStatus;                

                ////////////////////////// Test Code ////////////////////
                //SensorRecord srTest = null;
                //Debug.Print("---------------------------------REGISTERED SENSORS LIST---------------------------");
                //foreach (var sensor in _SensorList)
                //{
                //    srTest = sensor as SensorRecord;
                //    if (srTest !=null)
                //        Debug.Print(srTest.ToString() + "\n");
                //}

                //Debug.Print("---------------------------------END LISTS-----------------------------------------");
                ///////////////////////////////////////////////////////////
            }
            catch
            {
                //Swallow it
            }
        }

        public static void UpdateSensorRecordToDatabase()
        {
            if (_SensorList.Count <= 0)
                return;

            ResetWatchDog();

            //Update to database
            DB db = new DB();
            SensorRecord sr = null;
            if (db.OpenDB() == true)
            {
                foreach (var sensor in _SensorList)
                {
                    if (sensor is SensorRecord)
                    {
                        sr = sensor as SensorRecord;
                        db.UpdateSensorRecord(sr);
                    }
                }
                db.CloseDB();
            }

            ResetWatchDog();
        }

        public static int GetOldestBlock(string sensorSN)
        {
            HUtility util = new HUtility();
            _oldestBlock = -1;  //default
            //Send Get Log Data to sensor                                          
            _sendPacket = HTools.Hex.ToBytes(_REQ_DATA_CMD + sensorSN + "EE" + "FFFF");
            util.WriteToUART(_spCOM3, _sendPacket, false);

            //Wait until you get the right data block back
            _ps_download_count_1 = 0;
            _ps_download_count_2 = 0;

            while (true)
            {
                Thread.Sleep(1);
                _ps_download_count_1++;
                _ps_download_count_2++;

                if (_ps_download_count_2 > 3000)
                {
                    break;
                }

                if (_NewPointSixPackageReceived)  //If there is a new package received
                {
                    //SpecialGlobalVar._XBEE900SerialBuffer.CopyTo(_PointSixPackage, 0);
                    _NewPointSixPackageReceived = false;  //reset flag

                    string hexPackage = HTools.ByteArr.ToHex(SpecialGlobalVar._XBEE900SerialBuffer).ToUpper();

                    //Check to see if it is a Sensor Log Data Response, check for match serial number, transaction id, and requested memory location                                                
                    if (hexPackage.Substring(6, 2) == "22" && hexPackage.Substring(16, 2) == "EE" && hexPackage.Substring(8, 8) == sensorSN)
                    {
                        //Get oldest block
                        _oldestBlock = Convert.ToInt32(hexPackage.Substring(20, 2), 16);

                        //Debug.Print("Oldest Block Acquired: " + hexPackage);

                        break;
                    }
                }

                if (_ps_download_count_1 > 200)  //Resend command if requested package haven't comes in for a period of time
                {
                    util.WriteToUART(_spCOM3, _sendPacket, false);
                    _ps_download_count_1 = 0;
                }

            }//End while loop 

            if (_oldestBlock == -1)
                return -2;

            return _oldestBlock;
        }

        public static bool IsSensorDelay(SensorRecord oldRecord, SensorRecord newRecord, int delayInterval)
        {
            TimeSpan ts = newRecord.TimeStamp.Subtract(oldRecord.TimeStamp);
            int days_diff = ts.Days;
            int hours_diff = ts.Hours;
            int minutes_diff = ts.Minutes;

            if (days_diff > 0)   //Delay more than a day
                return true;
            else if (hours_diff > 0)  //Delay more than one hour
                return true;
            else if (minutes_diff > delayInterval)  //Delay more than specified minutes          
                return true;
            else
                return false;
        }

        public static long ToEpochTicks(DateTime dt){
            return dt.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).Ticks / TimeSpan.TicksPerSecond;
        }

        #endregion


    }   
     
}




 