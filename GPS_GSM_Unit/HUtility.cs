using System;
using System.Text;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using System.IO.Ports;
using System.Threading;
using GHI.Pins;

using HTools;

namespace GPS_GSM_Unit
{
    public enum LED_Blink_Type
	{
	    ON_OFF_ON = 0,
        ON_OFF = 1,
        OFF_ON = 2
	}

    public enum HUB_RESPONSE_CODE : int
    {
        OK_WITH_NO_ACTION_COMMAND = 0,
        NO_RESPONSE_3G_MODULE_TIMEOUT = 1,
        NO_RESPONSE_3G_MODULE_DID_NOT_TIMEOUT = 2,   
        OK_WITH_REQUEST_SETTING = 3,
        OK_WITH_SET_SETTING = 4,
        NOT_OK = 5,
        INVALID_DATA = 6
    }

    public class HUB_RESPONSE
    {
        public HUB_RESPONSE(HUB_RESPONSE_CODE reponseCode, byte[] reponseByteArray)
        {
            this.RESPONSE_CODE = reponseCode;
            this.ResponseByteArray = reponseByteArray;
        }

        public HUB_RESPONSE_CODE RESPONSE_CODE { get; set; }
        public byte[] ResponseByteArray { get; set; }
    }
    
    public class GPSData
    {
        public GPSData(DateTime utcDateTime, string longitude, string latitude, string altitude_in_meter, string speed_km_per_hour, string number_of_sat_in_use)
        {           
            this.UTCDateTime = utcDateTime;
            this.Longitude = longitude;           
            this.Latitude = latitude;           
            this.Altitude_In_Meter = altitude_in_meter;
            this.Speed_KM_Per_Hour = speed_km_per_hour;
            this.Number_Of_Sat_In_Use = number_of_sat_in_use;
        }
        
        public DateTime UTCDateTime { get; set; }        
        public string Longitude { get; set; }       
        public string Latitude { get; set; }       
        public string Altitude_In_Meter { get; set; }
        public string Speed_KM_Per_Hour { get; set; }
        public string Number_Of_Sat_In_Use { get; set; }
    }


    public class HUtility
    {
        public HUtility()
        { }
        
        
        ////////////////////
        // Helper methods //        
        /// ////////////////       

        public bool WriteToUART(SerialPort sp, string data, bool sendCRLF)
        {
            try
            {
                sp.DiscardOutBuffer();
                sp.Flush();
                byte[] byteData = Encoding.UTF8.GetBytes(data);
                sp.Write(byteData, 0, byteData.Length);
                if (sendCRLF)
                {
                    WriteCRLF(sp);
                }

                byteData = null;
                return true;
            }
            catch (Exception ex)
            {
                Debug.Print("From [WriteToUART (using string)]:" + ex.ToString());
                sp.DiscardOutBuffer();
                sp.DiscardInBuffer();
                sp.Flush();
                return false;
            }
        }

        public bool WriteToUART(SerialPort sp, byte[] data, bool sendCRLF)
        {
            if (data == null)
            {
                return false;
            }

            try
            {
                sp.Flush();
                sp.Write(data, 0, data.Length);                                               
                if (sendCRLF)
                {
                    WriteCRLF(sp);                   
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.Print("From [WriteToUART (using byte[] array)]: " + ex.ToString());
                sp.DiscardOutBuffer();
                sp.DiscardInBuffer();
                sp.Flush();
                return false;
            }
        }

        public byte[] ReadUART(SerialPort sp)
        {            
            if (sp.BytesToRead <= 0)
                return null;           
           
            int byteRead = 0;
            int totalByteRead = 0;
            
            try
            {                                
                while (sp.BytesToRead > 0)
                {
                    if (totalByteRead >= SpecialGlobalVar._3GModuleSerialBuffer.Length)
                            break;

                    if (sp.BytesToRead > (SpecialGlobalVar._3GModuleSerialBuffer.Length - totalByteRead))
                        byteRead = sp.Read(SpecialGlobalVar._3GModuleSerialBuffer, totalByteRead, SpecialGlobalVar._3GModuleSerialBuffer.Length - totalByteRead);
                    else
                        byteRead = sp.Read(SpecialGlobalVar._3GModuleSerialBuffer, totalByteRead, sp.BytesToRead);
                    
                    totalByteRead += byteRead;
                }

                byte[] readBuffer = new byte[totalByteRead];
                                
                Array.Copy(SpecialGlobalVar._3GModuleSerialBuffer, 0, readBuffer, 0, totalByteRead);

                sp.Flush();             
                return readBuffer;
            }
            catch (Exception ex)
            {
                Debug.Print("From [ReadUART]: " + ex.ToString()); 

                sp.DiscardOutBuffer();
                sp.DiscardInBuffer();
                sp.Flush();

                //buffer = null;
                return null;
            }
        }
        
        public string StringXOR(string inputString)
        {
            byte[] byteStr = Encoding.UTF8.GetBytes(inputString);
            int xorDec = 0;

            for (int i = 0; i < byteStr.Length; i++)
            {
                xorDec = xorDec ^ byteStr[i];
            }

            return xorDec.ToString("X");
        }

        public byte[] PassPharseXOR(byte[] data, byte[] xorPhrase)
        {
            byte[] result = new byte[data.Length];

            for(var i=0; i < data.Length; i++)
	        {
                result[i] = (byte)(data[i] ^ xorPhrase[i % xorPhrase.Length]);
	        }
            return result;
        }
        
        public void WriteCTRL_Z(SerialPort sp)
        {
            byte[] Ctrl_Z = new byte[] { 26 };
            sp.Write(Ctrl_Z, 0, Ctrl_Z.Length);
        }
       
        public void WriteCRLF(SerialPort sp)
        {
            byte[] byteCRLF = new byte[] { 13, 10 };
            sp.Write(byteCRLF, 0, byteCRLF.Length);
            byteCRLF = null;
        }

        //Return true if the uart byte array contain the string 'message'
        public bool UARTMessageContain(byte[] uartByteArray, string message)
        {
            if (uartByteArray == null)
                return false;

            if (uartByteArray.Length <= 0)
                return false;
            
            try
            {
                string[] lines = ByteArrayToStringLines(uartByteArray);
                int index = 0;
                foreach (var line in lines)
                {                               
                    //check for string message
                    index = line.IndexOf(message);
                    if (index != -1)
                    {

                        if (index == 0)
                        {
                            //make sure the string is not empty string
                            if (String.Compare(line.Trim(), String.Empty) == 0 || String.Compare(line.Trim(), "") == 0)
                                return false;
                            else
                                return true;
                        }
                        else
                        {
                            return true;
                        }                       
                    }
                }

                lines = null;
                return false;
            }
            catch
            {
                return false;
            }           
        }

        /// <summary>
        /// Return a string line that contain the "has_this_string" value
        /// </summary>
        /// <param name="buffer">The read UART byte buffer.</param>
        /// <param name="has_this_string">The string value.</param>
        /// <returns></returns>
        public string GetStringLineFromUARTMessage(byte[] uartByteArray, string has_this_string)
        {
            if (uartByteArray == null)
                return null;

            if (uartByteArray.Length <= 0)
                return null;

            try
            {
                string[] lines = ByteArrayToStringLines(uartByteArray);
                int index = 0;
                foreach (var line in lines)
                {
                    //check for string message
                    index = line.IndexOf(has_this_string);
                    if (index != -1)
                    {
                        if (index == 0)
                        {
                            //make sure the string is not empty string
                            if (String.Compare(line.Trim(), String.Empty) == 0 || String.Compare(line.Trim(), "") == 0)
                                return null;
                            else
                                return line.Trim();
                        }
                        else
                        {
                            return line.Trim();
                        }
                    }
                }

                lines = null;
                return null;
            }
            catch
            {
                return null;
            }           
        }
                
        public void BlinkLED(OutputPort ledPin, int blinkCount, int blinkDelay, LED_Blink_Type blinkType )
        {
            for (int i = 0; i < blinkCount; i++)
            {
                if (blinkType == LED_Blink_Type.ON_OFF_ON)
                {
                    ledPin.Write(true);
                    Thread.Sleep(blinkDelay);
                    ledPin.Write(false);
                    Thread.Sleep(blinkDelay);
                    ledPin.Write(true);
                    Thread.Sleep(blinkDelay);
                }
                else if (blinkType == LED_Blink_Type.ON_OFF)
                {
                    ledPin.Write(true);
                    Thread.Sleep(blinkDelay);
                    ledPin.Write(false);
                    Thread.Sleep(blinkDelay);
                }
                else
                {
                    ledPin.Write(false);
                    Thread.Sleep(blinkDelay);
                    ledPin.Write(true);
                    Thread.Sleep(blinkDelay);
                }
            }
        }

        public byte[] SanitizeForUTF8(byte[] byteData)
        {
            if (byteData == null)
                return null;

            if (byteData.Length <= 0)
                return null;
                             
            for (int i = 0; i < byteData.Length; i++)
			{
                if (byteData[i] == 10 || byteData[i] == 13)
                    continue;                                

                if (byteData[i] < 32 || byteData[i] > 127)
                    byteData[i] = 32;                
			}
            return byteData;
        }

        public byte[] SanitizedForValidHexBytes(byte[] byteData)
        {
            bool invalidByteDetected = false;
            if (byteData == null)
                return null;

            if (byteData.Length <= 0)
                return null;

            for (int i = 0; i < byteData.Length; i++)
            {
                if (byteData[i] >= 48 && byteData[i] <= 57)
                    continue;
                else if (byteData[i] >= 65 && byteData[i] <= 70)
                    continue;
                else if (byteData[i] >= 97 && byteData[i] <= 102)
                    continue;
                else
                {
                    invalidByteDetected = true;
                    break;
                }
            }

            if (invalidByteDetected == true)
                return null;
            else
                return byteData;
        }

        public byte[] FilterForValidHexBytes(byte[] byteData)
        {            
            if (byteData == null)
                return null;

            if (byteData.Length <= 0)
                return null;

            for (int i = 0; i < byteData.Length; i++)
            {
                if (byteData[i] == 10 || byteData[i] == 13)
                    continue;

                if (byteData[i] >= 48 && byteData[i] <= 57)
                    continue;
                else if (byteData[i] >= 65 && byteData[i] <= 70)
                    continue;
                else if (byteData[i] >= 97 && byteData[i] <= 102)
                    continue;
                else
                {
                    //replace invalid hex byte with a "line feed" byte 0x10
                    byteData[i] = 10;                    
                }
            }
           
            return byteData;
        }

        public byte[] SanitizePointSixSerialNoByteArray(byte[] byteData)
        {
            bool invalidSerial = false;
            if (byteData == null)
                return null;

            if (byteData.Length <= 0)
                return null;
            
            for (int i = 0; i < byteData.Length; i++)
            {
                if (byteData[i] >= 48 && byteData[i] <= 57)
                    continue;
                else if (byteData[i] >= 65 && byteData[i] <= 90)
                    continue;
                else
                {
                    invalidSerial = true;
                    break;
                }                    
            }            

            if (invalidSerial == true)            
                return null;            
            else            
                return byteData;
        }

        public bool SendCmdToUART(SerialPort sp, string cmd, string expectedReply, int waitPeriodBeforeCheckingForReply, bool sendCRLF)
        {
            byte[] uartResp = null;

            if (sendCRLF)
                WriteToUART(sp, cmd, true);
            else
                WriteToUART(sp, cmd, false);
            Thread.Sleep(waitPeriodBeforeCheckingForReply);
            uartResp = SanitizeForUTF8(ReadUART(sp));
            if (UARTMessageContain(uartResp, expectedReply) == true)
                return true;
            else
                return false;
        }

        public int GetTimeSpanSeconds(TimeSpan ts)
        {
            int totalSeconds = 0;
            totalSeconds = (ts.Hours *3600) + (ts.Minutes * 60) + ts.Seconds;
            return totalSeconds;
        }

        public string GetLongLatDecimal(string latitudeOrLongitude, bool isLatitude)
        {
            int sign = 0;
            if (isLatitude)
            {
                //if format is wrong
                if (latitudeOrLongitude.Length != 10)
                    return null;

                //Get sign
                if (latitudeOrLongitude[9] == 'N')
                    sign = 1;
                else if (latitudeOrLongitude[9] == 'S')
                    sign = -1;

                //Get degree
                double degree = double.Parse(latitudeOrLongitude.Substring(0, 2).Trim());
                //Get minutes
                double min = double.Parse(latitudeOrLongitude.Substring(2, 7).Trim());
                //Get decimal value
                double latDecimal = degree + (min / 60);

                return (latDecimal * sign).ToString();
            }
            else
            {
                //if format is wrong
                if (latitudeOrLongitude.Length != 11)
                    return null;

                //Get sign
                if (latitudeOrLongitude[10] == 'E')
                    sign = 1;
                else if (latitudeOrLongitude[10] == 'W')
                    sign = -1;

                //Get degree
                double degree = double.Parse(latitudeOrLongitude.Substring(0, 3).Trim());
                //Get minutes
                double min = double.Parse(latitudeOrLongitude.Substring(3, 7).Trim());
                //Get decimal value
                double longDecimal = degree + (min / 60);

                return (longDecimal * sign).ToString();
            }
        }
        
        public string[] ByteArrayToStringLines(byte[] byteArray)
        {            
            string gsmMsg = new string(Encoding.UTF8.GetChars(byteArray));
            string[] lines = gsmMsg.Split('\n');
            return lines;
        }
               
        public string ByteArrayToHexString(byte[] byteArray)
        {
            return ByteArr.ToHex(byteArray);
        }

        public byte[] HexStringToByteArray(string hexString)
        {
            return Hex.ToBytes(hexString);
        }

        public UInt64 ConvertToUInt64(string hex)
        {
            if (hex.Length <= 8)
            {
                return (UInt64)(UInt32)Convert.ToInt32(hex, 16);
            }
           
            hex = Str.PadLeft(hex, 16, '0');
            var high = hex.Substring(0, 8);
            var low = hex.Substring(8);

            return (ConvertToUInt64(high) << 32) | ConvertToUInt64(low);
        }
                

        /////////////////////////////////////////
        // 3G module and GPS Helper Methods    //
        /////////////////////////////////////////

        public bool InitializeComPort(SerialPort sp)
        {
            //Open COM port
            try
            {
                if (!sp.IsOpen)
                {
                    sp.Open();
                    return true;
                }
                else if (sp.IsOpen)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool Is3GModuleReady(SerialPort sp)
        {
            return SendCmdToUART(sp, "AT", "OK", 1000, true);
        }

        public bool Set_NO_FLOW_CONTROL_3G_MODULE(SerialPort sp)
        {
            return SendCmdToUART(sp, "AT&K0", "OK", 1000, true);
        }

        public void Reset3GModule(OutputPort resetPin)
        {
            resetPin.Write(false);
            Thread.Sleep(200);
            resetPin.Write(true);
            Thread.Sleep(5000);   //Wait 5 seconds for 3G module to power up after reset.            
        }

        /// <summary>
        /// This will hard reset the module and then make sure it is ready to receive AT command
        /// in no flow control mode with auto SIM detection.
        /// </summary>
        /// <param name="sp"></param>
        /// <param name="resetPin"></param>
        /// <returns></returns>
        public void Reset3GModuleWithMandatoryBasicSetup(SerialPort sp, OutputPort resetPin)
        {
            HUtility util = new HUtility();
            util.Reset3GModule(resetPin);

            //Check to make sure 3G module is power on and is ready for AT command
            while (util.Is3GModuleReady(sp) == false)
            {
                util.Reset3GModule(resetPin);
            }

            //3G module is ready to receive AT command, set no flow control to 3G module
            while (true)
            {
                if (util.Set_NO_FLOW_CONTROL_3G_MODULE(sp) == true)
                {
                    //Set SIM auto detection
                    util.SetSIMDetection(sp);
                    break;
                }
            }
        }
       
        public bool IsGPSPowerOn(SerialPort sp)
        {
            //Send this "AT$GPSACP" to see if GPS is power on
            return SendCmdToUART(sp, "AT$GPSACP", "$GPSACP:", 2000, true);   
        }

        public void SendGPSSetupCmd(SerialPort sp)
        {
            try
            {
                string[] gpsSetupCmd = new string[] { "AT$GPSSLSR=2,3,,,,,1", "AT$GPSNMUN=0,,,,,1,", "AT$GPSSAV" };

                foreach (string cmd in gpsSetupCmd)
                {
                    WriteToUART(sp, cmd, true);
                    Thread.Sleep(1000);
                }
            }
            catch
            {
                return;
            }
        }

        public bool SaveGPSParameters(SerialPort sp)
        {
            return SendCmdToUART(sp, "AT$GPSSAV", "OK", 2000, true);
        }

        /// <summary>
        /// Return GPS coordinate if locked, if not or gps coordinate is invalid, return null
        /// </summary>
        /// <param name="sp">Serial port 3G module connected to.</param>
        /// <returns>Return null or GPSData object containing gps data.</returns>
        public GPSData GetAcquiredGPSCoordinate(SerialPort sp)
        {
            WriteToUART(sp, "AT$GPSACP", true);
            Thread.Sleep(1000);
            byte[] buffer = ReadUART(sp);
            string gpsStringData = GetStringLineFromUARTMessage(buffer, "$GPSACP:");

            if (gpsStringData == null)
                return null;

            string[] gpsSplit1 = gpsStringData.Split(':');
            
            if (gpsSplit1[1].Length >= 2)
            {
                string[] gps = gpsSplit1[1].Split(',');
                if (gps.Length == 11)
                {              
                    //See if the GPS is fixed or locked
                    if (String.Compare(gps[5].Trim(), "0") == 0 || String.Compare(gps[5].Trim(), "1") == 0)
                        return null;                     
      
                    //Parse out the UTC Datetime
                    int year, month, day, hour, min, sec;
                    try
                    {
                        gps[9] = gps[9].Trim();
                        year = Convert.ToInt32(gps[9].Substring(4, 2)) + 2000;
                        month = Convert.ToInt32(gps[9].Substring(2, 2));
                        day = Convert.ToInt32(gps[9].Substring(0, 2));

                        gps[0] = gps[0].Trim();
                        hour = Convert.ToInt32(gps[0].Substring(0, 2));
                        min = Convert.ToInt32(gps[0].Substring(2, 2));
                        sec = Convert.ToInt32(gps[0].Substring(4, 2));
                    }
                    catch
                    {
                        return null;
                    }
                    //Get UTC data time
                    DateTime utcDT = new DateTime(year, month, day, hour, min, sec, 0); 
 
                     
                    GPSData gpsData = new GPSData(utcDT, 
                                                  gps[2].Trim(), 
                                                  gps[1].Trim(), 
                                                  gps[4].Trim(), 
                                                  gps[7].Trim(), 
                                                  gps[10].Trim());

                    return gpsData;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        

        ////////////////////////////////
        // 3G Cellular Helper Methods //
        ////////////////////////////////

        public bool SetSIMDetection(SerialPort sp)
        {
            if (SendCmdToUART(sp, "AT#SIMDET=1", "OK", 1000, true) == true)
                return true;
            else
                return false;
        }

        public bool Is3GModuleRegisteredToNetwork(SerialPort sp, out string networkStatus)
        {
            networkStatus = null;
            //Check to see if the module is register on a network
            //Send command to check for network "AT+CREG?"
            WriteToUART(sp, "AT+CREG?", true);
            Thread.Sleep(1000);
            byte[] byteMsg = SanitizeForUTF8(ReadUART(sp));

            if (byteMsg == null)
                return false;

            if (byteMsg.Length <= 0)
                return false;

            if (UARTMessageContain(byteMsg, "+CREG: 0,0") == true)
            {
                networkStatus = "NOT_REGISTERED_NOT_SEARCHING_FOR_NETWORK";
                return false;
            }
            else if (UARTMessageContain(byteMsg, "+CREG: 0,1") == true)
            {
                networkStatus = "REGISTERED";
                return true;
            }
            else if (UARTMessageContain(byteMsg, "+CREG: 0,2") == true)
            {
                networkStatus = "NOT_REGISTERED_BUT_SEARCHING_FOR_NETWORK";
                return false;
            }
            else if (UARTMessageContain(byteMsg, "+CREG: 0,5") == true)
            {
                networkStatus = "REGISTERED_ROAMING";
                return true;
            }
            else if (UARTMessageContain(byteMsg, "+CREG: 0,3") == true)
            {
                networkStatus = "REGISTRATION_DENIED";
                return false;
            }
            else if (UARTMessageContain(byteMsg, "+CREG: 0,4") == true)
            {
                networkStatus = "UNKNOWN";
                return false;
            }
            else
            {
                networkStatus = "UNKNOWN_RESPONSE_FROM_MODULE";
                return false;
            }
        }

        public bool SetPDPContext(SerialPort sp, string apnName)
        {
            string cmd = "AT+CGDCONT=1,\"IP\",\"" + apnName + "\"";
            return SendCmdToUART(sp, cmd, "OK", 1000, true);            
        }

        public bool ConfigureTCPStackParameters(SerialPort sp)
        {
            //socket connection id: 1
            //PDP context id: 1
            //packet size in bytes: 300
            //socket inactivity timeout: 15 seconds
            //connection timeout (can't connect to host timeout): 5 seconds
            //data sending timeout; after this period data are sent also if they�re less
            //than max packet size.  :
            return SendCmdToUART(sp, "AT#SCFG=1,1,300,30,50,1", "OK", 1000, true);
        }

        public bool IsPDPContextActivated(SerialPort sp)
        {
            return SendCmdToUART(sp, "AT#SGACT?", "#SGACT: 1,1", 1000, true);                
        }        

        public bool ActivatePDPContext(SerialPort sp)
        {
            return SendCmdToUART(sp, "AT#SGACT=1,1", "OK", 5000, true);            
        }

        public bool ActivatePDPContext(SerialPort sp, string apn_user_name, string apn_password)
        {
            return SendCmdToUART(sp, "AT#SGACT=1,1," + apn_user_name + "," + apn_password, "OK", 5000, true);            
        }

        public bool DeactivatePDPContext(SerialPort sp)
        {
            return SendCmdToUART(sp, "AT#SGACT=1,0", "OK", 1000, true);           
        }

        public bool EnableNetworkAutomaticUpdatingDateTime(SerialPort sp)
        {
            return SendCmdToUART(sp, "AT+CTZU=1", "OK", 1000, true);
        }

        public bool SetClockModeToUTCFormat(SerialPort sp)
        {
            return SendCmdToUART(sp, "AT#CCLKMODE=1", "OK", 1000, true);
        }

        public string GetSubscriberPhoneNumber(SerialPort sp)
        {
            byte[] uartResp = null;

            WriteToUART(sp, "AT+CNUM", true);
            Thread.Sleep(1000);
            uartResp = SanitizeForUTF8(ReadUART(sp));

            if (uartResp == null)
                return "";

            string[] msg = ByteArrayToStringLines(uartResp);
            if (msg.Length > 0)
            {
                foreach (var line in msg)
                {
                    //check for "+CNUM" response                    
                    if (line.IndexOf("+CNUM:") != -1)
                    {
                        StringBuilder sb = new StringBuilder(line.Split(',')[1].ToString().Trim());
                        sb.Replace("+", "").Replace('\"', ' ');
                        string cnum = sb.ToString().Trim();

                        uartResp = null;
                        msg = null;
                        return cnum;
                    }
                }
            }

            uartResp = null;
            msg = null;
            return "";
        }

        public int[] GetCellularNetworkDateTime(SerialPort sp)
        {
            byte[] uartResp = null;

            WriteToUART(sp, "AT#CCLK?", true);
            Thread.Sleep(1000);
            uartResp = SanitizeForUTF8(ReadUART(sp));

            if (uartResp == null)
                return null;

            string networkDT = GetStringLineFromUARTMessage(uartResp, "#CCLK:");

            if (networkDT == null)
                return null;

            try
            {
                int quo_index = networkDT.IndexOf('\"');
                string[] dt_array = networkDT.Substring(quo_index + 1, 17).Split(new char[] { ',' });
                string[] d_array = dt_array[0].Split(new char[] { '/' });
                string[] t_array = dt_array[1].Split(new char[] { ':' });

                int[] dt = new int[] { Convert.ToInt32(d_array[0]) + 2000, 
                                       Convert.ToInt32(d_array[1]), 
                                       Convert.ToInt32(d_array[2]), 
                                       Convert.ToInt32(t_array[0]), 
                                       Convert.ToInt32(t_array[1]), 
                                       Convert.ToInt32(t_array[2]) };

                return dt;
            }
            catch 
            {
                return null;
            }
                      
        }
        

        /////////////////////////////////////
        // TCP Transmission Helper Methods //
        /////////////////////////////////////
                               
        public bool OpenTCPSession(SerialPort sp, string hostIP, string portNumber)
        {            
            string cmd = "AT#SD=1,0," + portNumber + ",\"" + hostIP + "\"";
            return SendCmdToUART(sp, cmd, "CONNECT", 5000, true);           
        }

        /// <summary>
        /// Send message or command to HUB
        /// </summary>
        /// <param name="sp">3G Module serial port.</param>
        /// <param name="messageToSend">String message or command to send.</param>
        /// <param name="waitForHubResponseTimeoutInMilliseconds">The time to wait before checking serial port to see if there are any response/data from the HUB.</param>
        /// <param name="checkForHubResponseRetries">The number of time to retry to check for HUB response/data before method exit or end.</param>
        /// <returns>Returns the HUB_RESPONSE enum.</returns>
        public HUB_RESPONSE SendMsgOverTCPConnectionToHub(SerialPort sp, byte[] messageToSend, int waitForHubResponseTimeoutInMilliseconds, int checkForHubResponseRetries, bool sendCRLF)
        {                                   
            //Send the message
            if (sendCRLF)
                WriteToUART(sp, messageToSend, true);
            else
                WriteToUART(sp, messageToSend, false);
            
            //Check for hub msg response, and deal with it here
            for (int i = 0; i < checkForHubResponseRetries; i++)
            {
                Thread.Sleep(waitForHubResponseTimeoutInMilliseconds);
                byte[] uartResp = SanitizeForUTF8(ReadUART(sp));

                //Check to see if HUB response with "NOK", if so the package must be ignore because it is not valid to the HUB
                if (UARTMessageContain(uartResp, "NOK") == true)
                    return new HUB_RESPONSE(HUB_RESPONSE_CODE.NOT_OK, null);

                //Check to see if HOK present, if so Check to see if any command from HUB.                 
                if (UARTMessageContain(uartResp, "HOK") == true)
                {
                    //Check for HUB commands
                    if (UARTMessageContain(uartResp, "REQ_SETTING") == true)
                    {
                        return new HUB_RESPONSE(HUB_RESPONSE_CODE.OK_WITH_REQUEST_SETTING, uartResp);
                    }
                    else if (UARTMessageContain(uartResp, "SET_SETTING") == true)
                    {
                        return new HUB_RESPONSE(HUB_RESPONSE_CODE.OK_WITH_SET_SETTING, uartResp);
                    }
                    else
                    {
                        return new HUB_RESPONSE(HUB_RESPONSE_CODE.OK_WITH_NO_ACTION_COMMAND, uartResp);
                    }
                }

                //Check to see if INVALID present.                 
                if (UARTMessageContain(uartResp, "INVALID") == true)                
                    return new HUB_RESPONSE(HUB_RESPONSE_CODE.INVALID_DATA, null);
                
                
                //Check to see if 3G module no activity timeout
                if (UARTMessageContain(uartResp, "NO CARRIER") == true)
                     return new HUB_RESPONSE(HUB_RESPONSE_CODE.NO_RESPONSE_3G_MODULE_TIMEOUT, null); 

            }
                     
            return new HUB_RESPONSE(HUB_RESPONSE_CODE.NO_RESPONSE_3G_MODULE_DID_NOT_TIMEOUT, null);            
        }

        /// <summary>
        /// Send message or command to HUB, and exit, doesn't care about response from HUB
        /// </summary>
        /// <param name="sp"></param>
        /// <param name="messageToSend"></param>
        /// <returns></returns>
        public void SendMsgOverTCPConnectionToHub(SerialPort sp, byte[] messageToSend, bool sendCRLF)
        {            
            //Send the message
            if (sendCRLF)
                WriteToUART(sp, messageToSend, true);
            else
                WriteToUART(sp, messageToSend, false);             
        }

        public bool CloseTCPSession(SerialPort sp)
        {
            //Issue "+++" to switch back to command mode
            //Keep checking every 1 seconds for up to 5 seconds
            Thread.Sleep(2000);  //pause 2 seconds before issue +++
            byte[] uartResp = null;
            WriteToUART(sp, "+++", false);
            Thread.Sleep(2000);  //pause 2 seconds after issue +++

            for (int i = 0; i < 5; i++)
            {                              
                uartResp = SanitizeForUTF8(ReadUART(sp));
                if (UARTMessageContain(uartResp, "OK") == true)
                {   
                    //Module is now in command mode
                    Thread.Sleep(1000); //wait here for 1 seconds to make sure the module is for sure in command mode
                    //Issue close tcp socket
                    if (SendCmdToUART(sp, "AT#SH=1", "OK", 2000, true) == true)
                    {
                        Thread.Sleep(3000); //wait here another 3 seconds to make sure the socket is really closed
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }                                  
            }
            return false;
        }
        
        
    }
}
